<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Livbay\Item\AddTitleItem as AddTitleItem;
use Livbay\Title\Title as Title;
use Livbay\Rlist\Rlist as Rlist;
use Carbon\Carbon as Carbon;

class readingLists extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'books:readLists {title? : title ID}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update reading list information for titles';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $title = $this->argument('title');
        $today = Carbon::now()->toDateTimeString();
        if($title != null){
            $title = Title::find($title);

            /**
             * remove the current reading lists
             * @var [type]
             */
            //$title->removeRlists;
            
            
            $title->isbns->each(function($isbn){
                $isbn->rlists()->detach();
            });
            

            /**
             * check for more reading lists
             */
           // $title->createRlists;
            
            $rlist = collect();
            $title->isbns->each(function($isbn) use ($rlist){
                $r = new Rlist;
                $r->getRlists($isbn->isbn, "isbn");
                $rlist->push($r);
            }); 

            $this->comment($today. 'Reading list ID: '.$rlist->id.' updated');
            return;
        }
        else{

            /**
             * Process all titles
             */
            
            $titles = Title::all();

           // $bar = $this->output->createProgressBar(count($titles));

            $chunks = $titles->chunk(10);

            $chunks->each(function($titles) use ( $today){
                foreach($titles as $title){
                    
                    $title->isbns->each(function($isbn){
                        $isbn->rlists()->detach();
                    });

                    $rlist = collect();
                    $title->isbns->each(function($isbn) use ($rlist){
                        $r = new Rlist;
                        $r->getRlists($isbn->isbn, "isbn");
                        $rlist->push($r);
                    });
                    $this->comment($today.' Title id: '.$title->id.' reading lists updated');
                   // $bar->advance();
                }
            });

           // $bar->finish();
            return;

        }
    }
}
