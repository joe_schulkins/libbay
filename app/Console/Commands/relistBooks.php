<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Livbay\Item\Item as Item;
use Carbon\Carbon as Carbon;
use App\Events\ItemPurchaseCancelled as ItemPurchaseCancelled;
use Config;

class relistBooks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'books:relist {item? : the item ID}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Relist items not purchased';

    /**
     * Create a new command instance.
     *
     * @return void
     */
       public function __construct()
    {
        parent::__construct();
        
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $howlong = Config::get('bosbooks.daysToBuy');
        $item = $this->argument('item');
        $today = Carbon::now()->toDateTimeString();
        $logmsg = '';

        if($item != null){
            $item = Item::find($this->argument('item'));

            if($item->updated_at->lte(Carbon::now()->subDays($howlong))){
                \Event::fire(new ItemPurchaseCancelled($item));
            }
            else{
                if($this->confirm('Time isn\'t up for purchased. Do you still wish to continue? [y|N]')){
                    \Event::fire(new ItemPurchaseCancelled($item));
                }
                else{
                    return;
                }
            }
            $this->comment($today. 'Item ref: '.$item->ref.' relisted');
            return;
        }
        elseif($item === null){

            $updatedDate = Carbon::now()->subDays($howlong)->toDateTimeString();
            $items = Item::where('status','=','purchased')->where('updated_at', '<', $updatedDate)->get();

            if($items->count() == 0){
                $this->info('no items found');
                return;
            }
            else{
                $bar = $this->output->createProgressBar(count($items));

                $chunks = $items->chunk(10);

                $chunks->each(function($items) use ($bar, $today, $logmsg){
                     
                    foreach($items as $item){
                        \Event::fire(new ItemPurchaseCancelled($item));
                        $this->comment($today. 'Item ref: '.$item->ref.' relisted');
                        $bar->advance();
                    }
                });   
                
                $bar->finish();
                return;
            }
        }

        
    }
}
