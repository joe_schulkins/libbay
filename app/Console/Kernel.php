<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Carbon\Carbon as Carbon;
use Storage;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
       // \App\Console\Commands\Inspire::class,
        \App\Console\Commands\relistBooks::class,
        \App\Console\Commands\readingLists::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        
                $now = Carbon::now()->toDateString();

                $event = $schedule->command('books:relist')
                 ->before( function() use($now){
                    Storage::disk('local')->put('relist-'.$now.'.txt', '');
                 })
                 ->dailyAt('00:00')
                 ->sendOutputTo('./storage/app/relist-'.$now.'.txt');
                 
                 $schedule->command('books:readLists')
                 ->before( function() use($now){
                    Storage::disk('local')->put('readingLists-'.$now.'.txt', '');
                 })
                 ->weekly()
                 ->sundays()
                 ->at('00:00')
                 ->sendOutputTo('./storage/app/readingLists-'.$now.'.txt');
        
        
            
    }
}
