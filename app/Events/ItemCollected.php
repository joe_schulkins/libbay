<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ItemCollected extends Event
{
    use SerializesModels;

    public $userId;
    public $itemId;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($userId, $itemId)
    {
        $this->userId = $userId;
        $this->itemId = $itemId;
   //     \Tracker::trackEvent(['event'=>'item.collected']); 
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
