<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Livbay\Title\Title as Title;
use Livbay\Author\Author as Author;
use Livbay\Isbn\Isbn as Isbn;
use Livbay\User\User as User;
use Livbay\Item\Item as Item;
use Elasticsearch;
use Elasticsearch\ClientBuilder;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

class AdminSearchController extends Controller
{
    /**
     * to return results based on a search query using elasticsearch
     * @return [mixed] the view and array of results to return
     */
    public function adminSearch(){
    	/**
       *Check if user has sent a search query]
       * @var get all of the form input
       */
  		if($query = \Input::all()) {
        $type = $query['type'];
        $query = $query['query'];
        $client = ClientBuilder::create()->build();
        if($type == 'all'){
          $type = ['authors', 'isbns', 'items', 'titles', 'users'];
        }
		    $params= ['q' => $query, 'type'=>$type];
        $search = $client->search($params);

        /**
         * create an empty collection for our results
         * @var empty collection
         */
        $results = collect();

        /**
         * Iterate over results, pushing the type and 
         *  retrieved record to a collection and 
         *  push these to our result collection
         */
        foreach($search['hits']['hits'] as $hits){
          // the id of the item in our db
          $id = $hits['_id'];
        
          switch ($hits['_type']) {
            case 'users':
                $item = User::findOrFail($id);
                $item->id = $item->getRouteKey();
                $value = $item->fname.' '.$item->lname.' | '.$item->email ;
                break;            
            case 'titles':
                $item = Title::withTrashed()->findOrFail($id);
                $item->items;
                if(strlen($item->subtitle) > 0){
                  $value = $item->title.': '.$item->subtitle.' | '.$item->edition.' | '.$item->pubdate->year;
                }
                else{
                  $value = $item->title.' | '.$item->edition.' | '.$item->pubdate->year;
                }
                break;
            case 'authors':
                $item = Author::findOrFail($id);
                $value = $item->fname.' '.$item->lname;
                break;
            case 'items':
                $item = Item::withTrashed()->findOrFail($id);
                $value = $item->ref;
                break;
            case 'isbns':
                $item = Isbn::findOrFail($id);
                $value = $item->isbn;
                break;
          }
          $result = collect(['type'=>$hits['_type'], 'res'=>$item, 'value' => $value]);
          $results->push($result);
        }

        /** create a custom paginator **/
        $page = \Input::get('page', 1);
        $perPage = 10;
        $offset = ($page * $perPage) - $perPage;
        $results = new Paginator(array_slice($results->toArray(), $offset, $perPage, true), count($results), $perPage, $page);
        
        return view('admin.display-results', compact('results'));
      }
      
      
    }
}
