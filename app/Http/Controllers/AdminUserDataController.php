<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Livbay\User\User as User;
use Livbay\Item\Item as Item;
use Livbay\Title\Title as Title;
use Livbay\Rlist\Rlist as Rlist;
use Flash;
use DB;
use Elasticsearch;
use Elasticsearch\ClientBuilder;

use App\Events\ItemCollected;   
use App\Events\UserApproved;
use Config;

class AdminUserDataController extends Controller
{


     /**
     * Pages that only admin can see otherwise redirect
     */
      public function __construct(){
       $this->middleware('isadmin');
     }
     

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id, $type)
    {
         switch ($type) {
            case 'user':
            $id = \Hashids::decode($id)[0];
            $user = User::findOrFail($id);
            return redirect()->action('UserDataController@edit', [$user->id]);
                break;            
            case 'title':
            $title = Title::findOrFail($id);
            return redirect()->action('TitleDataController@edit', [$title->id]);
                break;
            case 'rlist':
          /**  $rlist = Rlist::findOrFail($id);
            return view('rlist.edit', compact('rlist'));
                break; **/
            case 'item':
            $item = Item::findOrFail($id);
            return redirect()->action('ItemDataController@edit', [$item->id]);
                break;
        }
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id, $type)
    {
        switch ($type) {
            case 'user':
            $user = User::findOrFail($id);
            $user->enquired;
            $user->listed;
           // $user->id = \Hashids::encode($user->id);
           // dd($user);
            return redirect()->action('UserDataController@destroy', [$user->id]);
                break;            
            case 'title':
            $title = Title::findOrFail($id);
            return redirect()->action('TitleDataController@destroy', [$title]);
                break;
            case 'rlist':
            $rlist = Rlist::findOrFail($id);
            return redirect()->action('RlistController@destroy', [$rlist]);
                break;
            case 'item':
            $item = Item::findOrFail($id);
            return redirect()->action('ItemDataController@destroy', [$item]);
                break;
        }
    }

    public function userMessage($id){

    }

    /**
     * ban a user
     * @param  int $id user id
     * @return [type]     [description]
     */
    public function ban($id)
    {

    }

    public function newUsers()
    {
        $user = User::sorted()->where('valid', '=', false)->paginate(7);
        $new_user = \Table::create($user, ['fname' => 'First Name', 'lname' => 'Last Name', 'email' => 'Email', 'created_at' => 'Added']);
        $new_user->setView('admin.new-user-table');

        return view('admin.new-user', compact('new_user'));
    }

    /**
     * approve a user
     * @param  int $id user id
     * @return [type]     [description]
     */
    public function approve($id)
    {
        $user = User::findOrFail($id);
        $user->valid = true;
        $user->save();
        Flash::message('User: <a href="/admin/edit/'.$user->id.'/'.$user->type.'">'.$user->fname.' '.$user->lname.'</a> has been approved.');

        // fire the event after an item is collected
        $response = \Event::fire(new UserApproved($user));
        Flash::message('User has been approved');
        return redirect()->back();
    }

    /**
     * make a user an admin
     * @param  int $id user id
     * @return [type]     [description]
     */
    public function makeAdmin($id)
    {
        $user = User::findOrFail($id);

        if(!$user->valid){
            $user->valid = true;
        }
        $user->administrator = true;
        $user->save();
        Flash::message('User: <a href="/admin/edit/'.$user->id.'/'.$user->type.'">'.$user->fname.' '.$user->lname.'</a> has been made an admin.');
        return redirect('home');
    }

    public function newItems(){
        $items = Item::sorted()->where('status', '=', 'review')->paginate(7);

        $items->each(function($item, $key){
            $item->title = ucwords($item->title->first()->title);
        });
       
        $new_item = \Table::create($items, ['title'=>'Title','ref' => 'Ref No.', 'condition' => 'Condition', 'price' => 'Price',  'created_at' => 'Added', ]);
        $new_item->setView('admin.new-item-table');

        return view('admin.new-items', compact('new_item','item'));
    }

    public function itemApprove($id)
    {
        $item = Item::findOrFail($id);
        $item->status = 'listed';
        $item->disp = 'show';
        $item->save();

        Flash::message('Item approved');

        return redirect()->back();
    }

    public function setToLocn($id, $locn){
        $item = Item::findOrFail($id);
        if($item->status != 'listed'){
            $item->status = 'listed';
        }
        $item->location = $locn;
        $item->save();
    }

    public function collectItem(){

        $items = Item::sorted()->with('enquired')->with('title')->where('status', '=', 'collect')->paginate(7);

        $items->each(function($item, $key){
            $item->title = ucwords($item->title->first()->title);
        });

        $items->each(function($item, $key){
            $item->user = ucwords($item->enquired->first()->fname) . " " . ucwords($item->enquired->first()->lname);
        });

        $col_item = \Table::create($items, ['title'=>'Title','ref' => 'Ref No.', 'price' => 'Price', 'user' => 'Bought By' ]);

        $col_item->addColumn('collect', 'Date For Collection', function($model){
            
            return $model->collect->format('d-m-Y');
        });

        $col_item->setView('admin.collect-item-table');

        return view('admin.adminCollect', compact('col_item'));
    }

    public function itemCollected($id){
        $item = Item::findOrFail($id);
        $user = $item->enquired()->first();
        $title = $item->title->first();
        $item->status = 'finished';
        $item->save();

        $item->delete();

        if($title->items->count() == 1){
            $d = new TitleDataController();
            $d->destroy($title->id);
        }

        $itemId = $id;
        $userId = $user->id;

        // fire the event after an item is collected
        $response = \Event::fire(new ItemCollected($userId,$id));
        
        Flash::message("Item has been collected");

        return redirect()->back();
    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function userDestroy($id)
    {
        $user = User::findOrFail($id);
        $listed = $user->listed->count();
        $enquired = $user->enquired->count();

        if($listed == 0 && $enquired == 0){
            $client = ClientBuilder::create()->build();
            // make sure we pick up the index dynamically rather than statically
            $index = Config::get('elasticquent.default_index');
            $params = ['index'=>$index, 'type'=>'users', 'id'=>$user->id];
            $client->delete($params);
            $user->delete();
            Flash::message('User deleted');
        }
        elseif($listed > 0 || $enquired > 0) {
            Flash::message('User was not deleted as they have active items');
        }
        else{
             Flash::message('Error');
        }
        return redirect()->back();
     }

     /**
      * display items to be removed from the shelf
      * @return [type] [description]
      */
     public function displayClearShelf(){
        $items = Item::sorted()->with('listed')->with(['title' => function($query){$query->withTrashed();}])->whereNotNull('deleted_at')->where('status', '!=', 'finished')->where('status', '!=', 'cleared')->paginate(7);

        $items->each(function($item, $key){
            return $item->title = ucwords($item->title->first()->title);
        });

        $items->each(function($item, $key){
            $item->user = ucwords($item->listed->first()->fname) . " " . ucwords($item->listed->first()->lname);
            $item->user_email = $item->listed->first()->email;
        });

        $col_item = \Table::create($items, ['title'=>'Title','ref' => 'Ref No.', 'price' => 'Price', 'user' => 'Seller', 'user_email' => 'Email' ]);

        $col_item->addColumn('deleted_at', 'Date Removed', function($model){
            
            return $model->deleted_at->format('d-m-Y');
        });

        $col_item->setView('admin.clear-shelf-table');

        return view('admin.clearShelf', compact('col_item'));
     }

     /**
      * remove items from the shelf and return to user 
      * @param  Request $request [description]
      * @return [type]           [description]
      */
     public function clearShelf($id){
        $item = Item::where('id',$id)->withTrashed()->restore();
        $item = Item::find($id);
        $item->status = 'cleared';
        $item->save();
        $item->delete();
        Flash::message('Item has been removed from shelf');
        return redirect()->back();
     }
}
