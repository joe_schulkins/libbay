<?php

namespace App\Http\Controllers\Auth;

use Livbay\User\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Mail;
use Config;



class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     * NB - see the Controller.php for additional routes that 
     * don't require authentication to use. 
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'fname' => 'required|max:255',
            'lname' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'pay' => 'required|max:255',
            'password' => 'required|confirmed|min:6',
            'terms_conditions' => 'required',
            'my_name'   => 'honeypot',
            'my_time'   => 'required|honeytime:5'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'fname' => $data['fname'],
            'lname' => $data['lname'],
            'email' => $data['email'],
            'pay' => $data['pay'],
            'uni_id' => $data['uni_id'],
            'password' => bcrypt($data['password']),
        ]);

        Mail::send('emails.user-registered', ['user'=>$user], function($m) use ($user){
                $m->from(Config::get('bosbooks.admin_email'), Config::get('bosbooks.name'));
                $m->to($user->email)->subject('New Registration');
        });

        $user->addToIndex();

        return $user;
    }
}
