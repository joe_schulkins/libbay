<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Livbay\Author\Author as Author;
use Elasticsearch;
use Elasticsearch\ClientBuilder;
use Config;

class AuthorDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $au = Author::findOrFail($id);
        $au->titles;
        $au->titles->first()->authors;
        $au->titles->first()->items;
        $au->titles->first()->isbns;

        $edit = $au->titles->first();

        return view('admin.detail-edit', compact('edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $au = Author::findOrFail($id);
        // remove from index
        $client = ClientBuilder::create()->build();
        // make sure we pick up the index dynamically rather than statically
        $index = Config::get('elasticquent.default_index');
        $params = ['index'=>$index, 'type'=>'authors', 'id'=>$au->id];
        $client->delete($params);

        $au->delete();

        return redirect()->back();
        
    }
}
