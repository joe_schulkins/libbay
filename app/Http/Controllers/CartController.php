<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Livbay\Item\Item as Item;
use Livbay\User\User as User;
use Flash;
use Cart;
use CodeItNow\BarcodeBundle\Utils\BarcodeGenerator;
use Notifynder;
use Mail;
use Config;

class CartController extends Controller
{

 // public $notAvailable;

  public function __construct(){
    $this->notAvailable = null;
  }

	/**
     * add an item to your cart
     * @param  var $id item id
     * @return [type]     [description]
     */
    public function addToCart($id){
        //is the user logged in?
        $user = \Auth::user();
        if( $user == null | false){
            return view('auth.login');
        }
        else{
            $item = Item::find($id);
            $it_ti = $item->title[0]->title;

            $search = Cart::search(['id' =>$item->id]);

            //dd($search);
            if( sizeof($search) > 0 && $search != false){
                Flash::message('This item is already in your basket');
            }
            elseif(sizeof($search) > 0 && $search == false){
            //  dd($search);
                Cart::add($item->id, $it_ti, 1, $item->price);
                Flash::message('Added \''.ucwords($it_ti).'\' to basket');
            }

            if($item->listed->first()->id == $user->id){
                Flash::message('You are selling this item');
            }
           
            return redirect()->back();
        }
    }

    /**
     * view your cart
     * @return obj returns the items in your cart and the total
     */
    public function viewCart(){
        $cart = Cart::content();
        $total = Cart::total();
        
        $notAvailable = $this->notAvailable;
        return view('cart.cart', compact('cart', 'total', 'notAvailable'));       
    }

    /**
     * remove an item from your cart given an id
     * @param  int $id rowId of item 
     * @return      updated cart collection
     */
    public function removeFromCart($id){
        Cart::remove($id);
        $cart = Cart::content();

        return redirect()->back();
    }

    public function checkCart($cart){
      $sold = collect();
      foreach($cart as $row){
        $item = Item::find($row->id);
        if($item->disp =='hide'){
          $sold->push($row);
          Cart::remove($row->rowid);
          continue;
        }
      }

      if($sold->count() == 0){
         return false;
        }
      elseif($sold->count() > 0){
          $this->notAvailable = $sold;
          return true;
      }
    }

    /**
     * process cart contents and send notifications
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function checkoutCart(Request $request){
  
        $user = \Auth::user();

        $cart = Cart::content();

        $gone = $this->checkCart($cart);

        if($gone == true){
          return $this->viewCart();
        }
        elseif($gone == false){

        foreach($cart as $row){
            $item = Item::find($row->id);
            if($item->enquired->count() == 0){
              $item->enquired()->attach([$user->id => ['relationship'=>'enquired']]);
            }
            else{
              die('There has been a problem processing your purchase');
            }
            $item->listed;
            $item->title;
            $item->status = 'purchased';
            $item->disp = 'hide';
            $item->save();

            $title = "'".ucwords($item->title->first()->title)."'";
            $ref = $item->ref;
            $seller = $item->listed->first();
   
            $data = [];
            $data['pay'] = $item->listed->first()->pay;
            $data['emails']['seller'] = $item->listed->first()->email;
            $data['emails']['buyer'] = $user->email;

            foreach ($data['emails'] as $key => $value) {
              if($key == 'buyer'){
                $data['recip'] = 'buyer';
                $sub = 'Item Bought';
              }
              else{
                $data['recip'] = 'seller';
                $sub = 'Item Sold';
              }
              Mail::send('emails.item-bought', ['data'=>$data, 'item'=>$item], function($m) use ($item, $data, $value, $sub){
                $m->from(Config::get('bosbooks.admin_email'), Config::get('bosbooks.name'));
                $m->to($value)->subject($sub);
                 });
            }

            /** Disable notifications
              // contact the seller
              Notifynder::category('item_sold')
              ->from(1)
              ->to($seller->id)
              ->url('')
              ->extra(compact('title', 'ref'))
              ->send();

              // contact the buyer
              Notifynder::category('item_bought')
              ->from(1)
              ->to($buyer->id)
              ->url('')
              ->extra(compact('title', 'ref'))
              ->send();

              // Notification for the library
              Notifynder::category('admin_item_bought')
              ->from($buyer->id)
              ->to(1)
              ->url('')
              ->extra(compact('title', 'ref', 'seller', 'buyer'))
              ->send();
            **/

 
          }

         Cart::destroy();
         Flash::message('Thank you for your interest in these titles. Please pay the seller promptly.');
         
          return redirect('home');
        }
    }


}