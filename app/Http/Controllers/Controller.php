<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

abstract class Controller extends BaseController
{
    use DispatchesJobs, ValidatesRequests;
/**
 * Check to see whether the person is authorized to view protected pages, if not
 * then they will be redirected to the login page.
 */
    public function __construct(){
    	// 'showTitle' and 'reading' refer to the methods as described in routes.php
        $this->middleware('auth', ['except'=>['index','about','contact','search','reading', 'show']]);
    }
}
