<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\TitleDataController as TitleDataController;
use Livbay\Item\Item as Item;
use Livbay\User\User as User;
use Livbay\Title\Title as Title;
use Flash;
use CodeItNow\BarcodeBundle\Utils\BarcodeGenerator;
use Carbon\Carbon;
use Elasticsearch;
use Elasticsearch\ClientBuilder;
use Notifynder;
use Mail;
use App\Events\ItemPurchaseCancelled;
use Config;



class ItemDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $item = new Item($request->all());

        Auth::user()->items()->save($item);

        return redirect('item');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $edit = Item::withTrashed()->where('id',$id)->first();
        $user = \Auth::user();
        /**if($edit->count() == 1){
            $edit = $edit->first();
        }**/
        $edit->title;
       /** else{
            die('Problem retrieving item');
        }**/
        // check whether to see the user is an admin or has listed the item
        if($user->admin || $user->listed->contains('id',$id)){
            return view('admin.detail-edit', compact('edit'));
        }
        else{
            return redirect('home');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $delete = false;
        $item = Item::withTrashed()->where('id',$id)->get()->first();
        //dd($item->deleted_at);
        if($item->deleted_at != null){
            $delete = true;
            $item->restore();
        }

        $dates = ['col'=>$request->collect, 'del'=>$request->delete];

        foreach($dates as $key => $date){
            //$date = explode(' ', $request->collect);
            //$date = explode('-', $date[0]);
            if($date != ''){
                if($key == 'col'){
                    $item->collect = Carbon::createFromFormat('d-m-Y', $date);
                }
                elseif ($key == 'del') {
                    $item->delete = Carbon::createFromFormat('d-m-Y', $date);
                }

            }
            else{
                if($key == 'col'){
                    $item->collect = null;
                }
                elseif ($key == 'del') {
                    $item->deleted_at = null;
                }
            }
        }
        $re = "/[£|p]/";
        $price = \Input::get('price');
        if(preg_match($re, $price)){
            $price = preg_replace($re, '', $price);
        }

        
        $item->comments = \Input::get('comments');
        $item->condition = \Input::get('condition');
        $item->status = \Input::get('status');
        if(\Input::get('status') == 'listed'){
            $item->disp = 'show';
        }
        //dd(\Input::get('price'));
        $item->price = $price;
        $item->save();
        $item->addToIndex();

        if($delete == true){
            $item->delete();
        }

        Flash::message('Item updated');
        return redirect('home');
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $user = \Auth::user();
        $item = Item::find($id);
        $del = $item;
        $title_id = $del->title->first()->id;
        if($del->enquired->count() > 0){
          Flash::message('Item has pending enquiries. It cannot be deleted until these are rejected.'); 
        }
        else{
            // remove from index
            $client = ClientBuilder::create()->build();
            // make sure we pick up the index dynamically rather than statically
            $index = Config::get('elasticquent.default_index');
            $params = ['index'=>$index, 'type'=>'items', 'id'=>$del->id];
            $delete = $client->delete($params);

            $del->delete();
            if(Title::findOrFail($title_id)->items->count() == 0){
                $d = new TitleDataController();
                $d->destroy($title_id);
            }

            if(!$user->admin){
                // send alert that an item was removed to admin
                $title = $item->title()->first()->title;
                $ref = $item->ref;
                Notifynder::category('admin_item_removed')
                ->from($user->id)
                ->to(1)
                ->url('')
                ->extra(compact('title', 'ref'))
                ->send();

                /** email admin
                $data['recip'] = 'seller';
                Mail::send('emails.item-removed', ['data'=>$data, 'user'=>$user, 'item'=>$item], function($m) use ($user, $item){
                $m->from(Config::get('bosbooks.admin_email'), Config::get('bosbooks.name'));
                $m->to($user->email)->subject('Item Sold');
                 }); **/
            }
            
            Flash::message('Item deleted');
        }
        return redirect()->back();
    }

    /**
     * Hide the specified resource from view
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function hide($id)
    {
        $hide = Item::find($id);
        if($hide->disp != 'hide'){
            $hide->disp = 'hide';
            Flash::message('Item hidden');
        }
        else{
           $hide->disp = 'show';
           Flash::message('Item visible'); 
        }
        $hide->save();

        // redirect
        
        return redirect()->back();
    }

    /**
     * Send a message about the item
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function message($id)
    {
        //
    }

    public function printSlip($id){
        $item = Item::find($id);
        $item->listed;
        $item->enquired;
        $item->title;
        $barcode = new BarcodeGenerator();
        $barcode->setText($item->ref);
        $barcode->setScale(2);
        $barcode->setType(BarcodeGenerator::Code11);
        $code = $barcode->generate();
        return view('partials.print-slip', compact('item','code'));
    }
    
    public function collectItem($id){
        
        $item = Item::findOrFail($id);
        $title = $item->title->first();
        
        // return information to be printed in a pickup sheet
        return view('user.collect', compact('title','item'));
    }

    public function cancelItem($id, $uid){
        $item = Item::findOrFail($id);
        /**$item->enquired()->detach($uid);
        $item->status = 'listed';
        $item->disp = 'show';
        $item->save();**/

        $user = User::findOrFail($uid);

        \Event::fire(new ItemPurchaseCancelled($item));
        Flash::message('Item purchase has been cancelled');
        return redirect('home');
    }

    /**
     * method to set collection status and date
     * @param Request $request form input (date field)
     * @param int  $id      Current item id
     */
    public function setCollectAttr(Request $request, $id){
        $item = Item::findOrFail($id);
        $date = explode('-', $request->date);
        // date needs to in the format ('yyyy', 'mm', 'dd', 1) 
        $item->collect = Carbon::create($date[2], $date[1], $date[0],1);
        $item->status = 'collect';
        $item->save();
        $user = \Auth::user();
        $user->id = $user->getRouteKey();
        Flash::message('Collection date set for ' . $item->collect->toDateString());
        return redirect('home');
    } 

    


    

    /**
     * display information about who has enquired about an item
     * @param  int $id item's id
     * @return [type]     [description]
     */
    public function enquiries($id){
        $item = Item::find($id);
        $title = $item->title;
        $enq_a =[];
        foreach($item->enquired as $enq){
            $enq_u['user'] = \Hashids::encode($enq->id);
            $enq_u['time'] = $enq['created_at']->toDateTimeString();
            $enq_u['eid'] = $item->id;
            array_push($enq_a, $enq_u);
        }

        return view('user.enq-users', compact('title','enq_a'));
    }

    /**
     * reject enquiry
     * @param  int $uid user id
     * @param  int $eid enquiry id
     * @return [type]     [description]
     
    public function reject($uid, $eid, $title){
        //send message to user that their request was rejected
        $user = User::findOrFail($uid);
        $item = Item::findOrFail($eid);
        Mail::send('emails.reject-enquire', ['user' => $user, 'title'=>$title], function($m) use ($user){
            $m->from('j_schulkins@hotmail.com', 'Livbay');
            $m->to('jschulkins@gmail.com', $user->name)->subject('Enquiry Rejected');
        });

        //remove the link in the item_user table for the enquiring user 

    }
    */
}
