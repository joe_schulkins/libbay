<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Livbay\Item\Item as Item;
use Livbay\User\User as User;
use Flash;

class NotifyController extends Controller
{
	/**
	 * show all notifications
	 * @param  int $id user id
	 * @return object     object of all notifcaitions
	 */
	public function allNotifications($id)
    {
        $user = User::find($id);
        $rows = $user->getNotifications($paginate = 7);
        if($rows->count() > 0){
          $rows->each(function($item, $key){             
                $cat = DB::table('notification_categories')
                        ->where('id','=',$item->category_id)
                        ->first();
                $item->category = $cat->name;
                if($key != 0){
                    $item->selected = false;
                }
                else{
                  $item->selected = true;
                  $read = DB::table('notifications')->where('id', $item->id)->update(['read' => 1]);
                }
            });

        $row = $rows->first()->text;
        }
        else{
          $row = null;
        } 
         
       // $read = DB::table('notifications')->where('id', $row->id)->update(['read' => 1]);
        $table = \Table::create($rows, ['read' => 'Read', 'category' => 'Subject', 'extra' => 'Details']);
        $table->setView('user.notifications-table');
        return view('user.notifications', compact('table', 'row'));

    }

     public function newNotifications($id)
    {
        $user = User::find($id);
        

    }

    /**
     * return details of one notification
     * @param  int $id   notification id
     * @return obj      notification object
     */
    public function displayNotification($id)
    {
      dd($id);
        $row = DB::table('notifications')->where('id', '=', $id)->get();
        return redirect()->back()->with('row');
    }

    public function deleteMsg($id){
      $row = DB::table('notifications')->where('id', '=', $id)->delete();
      Flash::message('Message deleted');
      return redirect()->back();
    }

}