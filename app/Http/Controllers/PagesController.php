<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Livbay\Rlist\Rlist as Rlist;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Flash;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

class PagesController extends Controller
{
    /**
     * contact page
     * @return view
     */
    public function contact()
    {
    	return view('pages.contact');
    }

    public function cancel()
    {
        return redirect()->back();
    }

    /**
     * about page 
     * @return view
     */
    public function about()
    {
    	return view('pages.about');
    }

    /**
     * main search box homepage and return all reading lists 
     * and how many items are on each
     * @return view
     */
    public function search()
    {

    $rlists = Rlist::all();

/**    $rlists->load([
        'title' => function($query){
            $query->withTrashed();
        }]);
**/

    foreach($rlists as $rlist){
        $rlist->titles();
    }

    
    $sorted = $rlists->filter(function($title){
        $cnt = $title->book->first();
        if($cnt){
            return $cnt->showItems()->count() > 0;
        }
        else{
            return;
        }
    });
    

    // sort reading lists by most titles on a list
    $sorted = $sorted->sortByDesc(function ($value, $key){
        return $value->rlist_name;
    });
    $rlists = $sorted->values()->all();

    // create a custom paginator
    $page = \Input::get('page', 1);
    $perPage = 7;
    $offset = ($page * $perPage) - $perPage;
    $rlists = new Paginator(array_slice($rlists, $offset, $perPage, true), count($rlists), $perPage, $page);

       return view('pages.search', compact('rlists'));
    }

    /**
     * set contents as false so we can select from a list of files to edit
     * @return [type] [description]
     */
    public function editText(){
        $contents = false;
        return view('admin.file-editor', compact('contents'));
    }

    /**
     * get the file to edit and return this to the editor
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function editThisText(Request $request){
        $path = base_path('resources/views/partials/'.$request->status.'.blade.php');
        $contents = \File::get($path);

        return view('admin.file-editor', compact('contents', 'path'));
    }

    /**
     * save the updated text and flash a message
     * @param  Request $request the changed text
     * @return [type]           [description]
     */
    public function saveText(Request $request){
        $file = $request->path;
        $contents = $request->tinymce;
        $old = \File::get($file);

        // lets decode any laravel expressions so they can be interpreted by blade
        $bytes_written = \File::put($file, \Html::decode($contents));

        if ($bytes_written === false){
            die("Error writing to file");
        }

        if($old != $contents ){
            Flash::message('File contents updated');
        }
        else{
            Flash::message('No changes made');
        }

        return redirect()->back();
    }
}
