<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Livbay\Rlist\Rlist;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class RlistController extends Controller
{
	 /**
     * get all the reading lists and associated ISBNs
     * @return collection of lists and isbns
     */
    public function showAll(){
    	$lists = Rlist::all();
      	foreach ($lists as $list){
        	$list->isbns;
      	}
    	return view('partials.reading_list', compact('lists'));
    }

    public function destroy($id){
        $rlist = Rlist::findOrFail($id);
        $rlist->delete();
        Flash::message('Reading list deleted');
        return redirect()->back();
    }

}
