<?php

namespace App\Http\Controllers;

use DB;
use Flash;
use Search;


use Livbay\Title\Title;
use Livbay\Isbn\Isbn;
use Livbay\Item\Item;
use Livbay\Author\Author;
use Livbay\Item\AddTitleItem as AddTitleItem;


use BibInfo\Copac as Copac;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Requests\BibRequest;
use App\Http\Controllers\Controller;
use Elasticsearch;
use Elasticsearch\ClientBuilder;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Config;

class TitleDataController extends Controller
{
    /**
     *  Auth middleware to check whether someone is logged in before proceeding to create/add 
     *  items.
     * 
     */
    
    public function index(){
        $titles = Title::all();
        $search = null;
        $arr = [];

        // we only want titles where there are items to display
        $filter = $titles->filter(function($title){
            return $title->showItems()->count() > 0;
        });

        // sort by title
        $filter = $filter->sortBy('title');
        $filter->values()->all();

        foreach($filter as $title){
            array_push($arr, $title);
        }
        //$arr = $titles->toArray();
        // create a custom paginator
        $page = \Input::get('page', 1);
        $perPage = 7;
        $offset = ($page * $perPage) - $perPage;
        $titles = new Paginator(array_slice($arr, $offset, $perPage, true), count($arr), $perPage, $page);
        $titles->setPath('title');
       // $search = \Request::get('query');
        
        return view('title.index',compact('titles','search'));
    }

    /**
     * show title and its associated details
     * @param  int $id title ID
     * @return title, items and reading lists
     */
    public function show($id){
        // findOrFail will throw an exception if ISBN can't be found
        $title = Title::with('authors')->with('isbns')->with('items')->findOrFail($id);

        if(\Auth::user() !== null){
            $user = \Auth::user();
            if($user->administrator == 1){
                $admin = true;
            }
            else{
                $admin = false;
            }
            $user = $user->id;
        }
        else{
            $user = null;
            $admin = false;
        }

        $filtered = $title->items->filter(function ($item) {
            if($item['attributes']['disp'] == 'show' && $item['attributes']['status'] == 'listed'){
                return $item;
            }
        });

        $lists = collect();
        foreach($title->isbns as $isbn){
            $lists->push($isbn->rlists->all());
        }
        $lists = $lists->collapse();
        $lists = $lists->unique('rlist_url');
        
        return view('title.show', compact('title','lists','user', 'admin'));
    }

    /*
    * Display new bibliographic record form
    *
    * @return response
    */
    public function create(){  
        return view('title.search');
    }

    /*
    * Edit a bibliographic record
    *
    * @return response
    */
    public function edit($id){
        $edit = Title::with(['items','authors','isbns'])->where('id',$id)->first();
        $edit->items->each(function($item){
            $item->listed;
            $item->enquired;
            return $item;
        });

        return view('admin.detail-edit', compact('edit'));
    }

  /*
    * Search for a bibliographic record
    *
    * @param string
    * @return response
    *
    */
    public function search(){

                  
    }

    /**
    * Save a new bibliographic record
    *
    *  typehint $request to use our validation 
    * 
    * @param CreateBibRequest $request
    * @return response
    *
    **/
    public function store(Request $data){   

    }


    public function destroy($id){
        // check titles don't have any items
        $titles = Title::findOrFail($id);
        if($titles->items->count() > 0){
            return;
        }

        $client = ClientBuilder::create()->build();
        // make sure we pick up the index dynamically rather than statically
        $index = Config::get('elasticquent.default_index');
        $params = ['index'=>$index];

        // remove isbns
        $titles->isbns->each(function($val, $key) use ($client, $params){

            // remove from index
            $params['type'] = 'isbns';
            $params['id'] = $val->id;
            $client->delete($params);

            Isbn::destroy($val->id);
        });
        $titles->isbns()->detach($id);

        // remove authors
        $titles->authors->each(function($val, $key) use ($client, $params){
            // check to see if the authors arre on any other title
            $count = $val->titles->count();
            if($count == 1){
                // remove from index
                $params['type'] = 'authors';
                $params['id'] = $val->id;
                $client->delete($params);
                Author::destroy($val->id);
            }
        });
        $titles->authors()->detach($id);

        // remove from index
        $params['type'] = 'titles';
        $params['id'] = $titles->id;
        $client->delete($params);

        // delete the title
        $titles->delete();

        return redirect('home');

    }

    public function update($id, BibRequest $request){
        dd(Input::get());
        $bib = Title::findOrFail($id);
        $bib->update($request->all());
        return view('title.edit', compact('bib'));
    }
    
    /**
     * [findIsbn description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function findIsbn(request $request){
        $user = \Auth::user()->id;
        $title = new AddTitleItem($request->isbn, $user);
        if($title->exists == true){
            $this->title = $title->title;
        }
        elseif($title->exists == false){
            $this->title = $title->searchForTitle($request->isbn); 
        }
        
        $exists = $title->exists;
        \Session::put('title', $title);
        $title = $title->title;

        if(gettype($title) == 'array'){
            if(array_key_exists('edition', $title) == false){
                $title['edition'] = '';
            }
            if(array_key_exists('pubdate', $title) == false){
                $title['pubdate'] = '';
            }
        }

        if($title == null){
            Flash::message('There has been an error loading the details, please enter them manually');
            return $this->createManualTitle($request);
        }

        if(array_key_exists('isbns', $title) == false||$title['isbns'] == false){
            Flash::message('No data for isbn: '.$request->isbn);
            return $this->createManualTitle($request);
        }
        else{ 
            return view('title.create', compact('title','exists'));
        }

    }

    /**
     * create a manual title
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function createManualTitle(Request $request){
        $title['isbns'] = false;
        $exists = false;
        $edit = new \stdClass();
        $edit->authors = [];
        $edit->isbns = [];
        $edit->edition = '';
        $edit->volume = '';
        $edit->pubdate = new \stdClass();
        $edit->pubdate->year = '';
        $sess = new \stdClass();
        $sess->title['isbns'] = false;
        $sess->userID = \Auth::user()->id;
        \Session::put('title', $sess);
        return view('title.create', compact('title', 'exists', 'edit'));
    }

    /**
     * create a title and add an item
     * @param Request $request [description]
     */
    public function addTitleItem(Request $request){

        $title = \Session::get('title');  

        if($title->title['isbns'] == false){
            $isbns = \Input::get('isbn');
            $isbns = $isbns[0]['isbn'];
            $title->title['title'] = \Input::get('title');
            $title->title['subtitle'] = \Input::get('subtitle');
            $title->title['authors'] = \Input::get('author');
            $title->title['isbns'][0] = $isbns;
            $title->title['edition'] = \Input::get('edition');
            $title->title['volume'] = \Input::get('volume');
            $title->title['pubdate'] = \Input::get('pubdate');
        }
        
        $TitleItem = new AddTitleItem($title->title['isbns'][0], $title->userID);
        $TitleItem->updateTitle($title->title);
        $TitleItem->addTitle($request);
        \Session::forget('title');

        Flash::message('Your item has been added.');
        return redirect('home');
    }

    /**
     * create an item
     * @param Request $request item specifics
     * @param int  $userID  user ID
     * @param int  $titleID title ID
     */
    public function addNewItem(Request $request){
        $title = \Session::get('title');
        $TitleItem = new AddTitleItem($title->title['isbns'][0], $title->userID);
        $TitleItem->addItem($request, $title->userID, $title->titleID);
        \Session::forget('title'); 
        return redirect('home');
    }

    
    /**
     * Return list of titles on a reading list in libbay
     * @param  int $id rlist id
     * @return array   array of titles belonging to a reading list
     */
    public function reading($id){

        $r = \Livbay\Rlist\Rlist::with('isbns')->where('id',$id)->first();

        $col = $r->isbns->each(function ($isbn, $key){
         $isbn->title = $isbn->titles->first()->title;
         $isbn->subtitle = $isbn->titles->first()->subtitle;
         $isbn->edition = $isbn->titles->first()->edition;
         $isbn->volume = $isbn->titles->first()->volume;
         $isbn->pubdate = $isbn->titles->first()->pubdate;
         $isbn->id = $isbn->titles->first()->id;
        });

        $col = $r->isbns->unique('title');

        // only show titles if there are items available
        $col = $col->filter(function($title){
                    return $title->titles->first()->showItems()->count() > 0;
                });

        $col->values()->all();
        $col = $col->toArray();
        // create a custom paginator
        $page = \Input::get('page', 1);
        $perPage = 7;
        $offset = ($page * $perPage) - $perPage;
        $col = new Paginator(array_slice($col, $offset, $perPage, true), count($col), $perPage, $page);
        $col->setPath($id);
        return view('title.readinglist', compact('r', 'col'));
    }

}
