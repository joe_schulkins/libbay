<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Livbay\Title\Title as Title;
use Livbay\Author\Author as Author;
use Livbay\Isbn\Isbn as Isbn;
use Elasticsearch;
use Elasticsearch\ClientBuilder;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Config;

class TitleSearchController extends Controller
{
    /**
     * to return results based on a search query using elasticsearch
     * @return [mixed] the view and array of results to return
     */
    public function index(){
    	// Check if user has sent a search query
  		if($query = \Input::get('query', false)) {
    	$client = ClientBuilder::create()->build();
      // make sure we pick up the index dynamically rather than statically
      $index = Config::get('elasticquent.default_index');
		$params= ['index'=>$index,
               'type' => [
                  'authors', 'isbns', 'titles', 'rlist'
                ],
                'body'=>[
                  'query' =>[
                    'multi_match' => [
                      
                      'query' => $query,
                      'fields' => [
                        'title^1', 'subtitle', 'fname', 'lname', 'isbn', 'rlist_name'
                        ],
                        'fuzziness' => 'AUTO'
                      ]
                    ]
                  ]
               ];
    	$search = $client->search($params);
      //dd($search);
    	$arr = [];
      $collect = collect();

    	foreach($search['hits']['hits'] as $hits){

        foreach($hits['_source'] as $key => $value){
                 
          
          if($key == 'id'){
            $id = $value;
          }

    		if($hits['_type'] == 'authors'){     
    			$author = Author::find($id);

          if(gettype($author) == "object"){
    			$author->titles;
          $titleID = $author->titles->first();
          $titleID->isbns;
          }
          elseif(gettype($author) == "array"){
            var_dump($author);
          }         
         $collect->push($titleID);
    		}
    		if($hits['_type'] == 'isbns'){
    			$isbn = Isbn::find($id);
    			$titleID = $isbn->titles->first();
          //$titleID = $titleID->t;
          $collect->push($titleID);
    		}
    		if($hits['_type'] == 'titles'){
            $title = Title::find($hits['_source']['id']);
            $title->isbns;
            $collect->push($title);

    		}
        if($hits['_type'] == 'rlist'){
            var_dump($id);
            $title = Rlist::find($id);
            $title->isbns->titles->first();
            $collect->push($title);

        }
      }
  }

// remove duplicates
$filter = $collect->unique('id');


// we only want titles where there are items to display
$filter = $filter->filter(function($title){
    return $title->showItems()->count() > 0;  
});


 // move to array for the paginator method to work
 foreach($filter as $title){
  array_push($arr, $title);
 }
 
    // create a custom paginator
    $page = \Input::get('page', 1);
    $perPage = 7;
    $offset = ($page * $perPage) - $perPage;
    $titles = new Paginator(array_slice($arr, $offset, $perPage, true), count($arr), $perPage, $page);
    $titles->setPath('search');
    $search = \Request::get('query');

  		} 
  		else {
    		// Show all if no query is set
    		$titles = Title::paginate(10);
        $search = null; 
  		}
      if(sizeof($titles) != 0){
  		 return view('title.index', compact('titles','search'));
      }
      else{
       return view('title.none');
      }
      
    }
}
