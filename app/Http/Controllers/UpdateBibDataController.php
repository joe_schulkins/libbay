<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

// reference the namespace
use Livbay\User\User as User;
use Livbay\Title\Title as Title;
use Livbay\Item\Item as Item;
use Livbay\Isbn\Isbn as Isbn;
use Livbay\Author\Author as Author;
use Illuminate\Support\Facades\Session;

use App\Http\Requests;
use App\Http\Requests\BibRequest;
use App\Http\Controllers\Controller;
use Flash;

class UpdateBibDataController extends Controller{

	public function addFields(Request $request, $type){
		dd($type);
	}

	public function update(Request $request, $id){
		$title = Title::findOrFail($id);

		$authors = \Input::get('author');

		$au_sync_ids = [];

		foreach($authors as $author){

			if(array_key_exists('id', $author)){
				// update author and save changes
				$au = Author::findOrFail($author['id']);
				$au->lname = trim(mb_strtolower((string)$author['lname'], 'UTF-8'));
				$au->fname = trim(mb_strtolower((string)$author['fname'], 'UTF-8'));
				$au->save();

				// push the id to the sync array
				array_push($au_sync_ids, $au->id);

				// update the index
				$au->addToIndex();
			}
			elseif(array_key_exists('newid', $author)){
				$au = new Author;
				$au = $au->addAuthorToTitle($author, $title->id);
				array_push($au_sync_ids, $au->id);
			}
		}
		$title->authors()->sync($au_sync_ids);

		$isbns = collect(\Input::get('isbn'));

		$isb_sync_ids = [];

		foreach ($isbns as $isbn) {
			if(array_key_exists('id', $isbn)){

				$i = Isbn::findOrFail($isbn['id']);
				$ib_reg = preg_replace("/[^0-9xX].+/", "",(string)$isbn['isbn']);
          		$isbn13 = Isbn::isbn13(trim($ib_reg));
          		$isbn = new \Isbn\Isbn();
          		$isbn->hyphens->removeHyphens($isbn13);

          		if(strlen($isbn13) < '13' ){
              		$isbn13 = $isbn->translate->to13($isbn13);
              		$isbn13 = (string)$isbn13;
           		}
				$i->isbn = $isbn13;
				$i->save();

				// push the id to the sync array
				array_push($isb_sync_ids, $i->id);

				// update the index
				$i->addToIndex();
			}
			elseif(array_key_exists('newid', $isbn)){
				$i = new Isbn();
				$i = $i->addIsbnToTitle($isbn['isbn'], $title->id);
				array_push($isb_sync_ids, $i->id);
			}
		}
		$title->isbns()->sync($isb_sync_ids);


		$title->title = \Input::get('title');
		$title->subtitle = \Input::get('subtitle');
		$title->edition = \Input::get('edition');
		$title->volume = \Input::get('volume');
		$title->pubdate = \Input::get('pubdate');
		$title->save();

		$title->addToIndex();
		
		return redirect()->action('TitleDataController@show', [$title->id]);
	}


} 