<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

// reference the namespace
use Livbay\User\User as User;
use Livbay\Title\Title as Title;
use Livbay\Item\Item as Item;

use App\Http\Requests;
use App\Http\Requests\BibRequest;
use App\Http\Controllers\Controller;
use Flash;
use Elasticsearch;
use Elasticsearch\ClientBuilder;
use Config;

class UserDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $user = \Auth::user();
        if($user != null){

        $user = User::find($user->id);

        if($user->admin == true){
            $item = Item::sorted()->where('status', '=', 'review');
            $admin['i_count'] = $item->count();

            $admin['u_count'] = User::sorted()->where('valid', '=', false)->count();

            $admin['collect'] =  Item::sorted()->where('status', '=', 'collect')->count();

            $admin['clr'] = DB::table('items')->whereNotNull('deleted_at')->where('status', '!=', 'finished')->where('status', '!=', 'cleared')->count();

            $user->listed;
            $user->enquired;

            return view('admin.index', compact('admin', 'user'));
        }
        elseif($user->valid == true){
            $user->listed;
            $user->enquired;
            //$user->getNotificationsNotRead($limit = null, $paginate = 4, $order = 'desc');
            $user->id = $user->getRouteKey();
            return view('user.home', compact('user'));
        }
        else{
            return view('user.notsetup');
        }
        }
        else{
            return view('auth.login');
        }
    }

    public function listed($id)
    {
        $user = User::find($id);
        $l = $user->listed;
        $listed = [];
        foreach($l as $items){
            $item = Item::find($items->pivot->item_id);
            $item->enquired->count();
            array_push($listed, $item);
        }
        $user->id = $user->getRouteKey();
        return view('user.listed', compact('user','listed'));
    }

    /**
     * [edit description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function edit($id){
        //dd($id);
        $edit = User::findOrFail($id);

        $listed = Item::sorted()->join('item_user', 'items.id', '=', 'item_user.item_id')->join('users','item_user.user_id', '=', 'users.id')->join('item_title','item_title.item_id', '=', 'items.id')->where('users.id','=',$edit->id)->where('relationship', '=', 'listed')->paginate(5);
        $table = \Table::create($listed, ['ref' => 'Ref. No.', 'condition' => 'Condition', 'price' => 'Price', 'comments'=>'Comments', 'status' =>'Status' ,'relationship'=>'relationship'] );
        $table->addColumn('title', 'Title', function($model){
            $model->id = $model->item_id;
            return ucwords($model->getTitle());
        });

        $table->setView('user.edit-user-table');
        $edit->listed = $table;

        $enquired = Item::sorted()->join('item_user', 'items.id', '=', 'item_user.item_id')->join('users','item_user.user_id', '=', 'users.id')->join('item_title','item_title.item_id', '=', 'items.id')->where('users.id','=',$edit->id)->where('relationship', '=', 'enquired')->paginate(5);
        $table = \Table::create($enquired, ['ref' => 'Ref. No.', 'condition' => 'Condition', 'price' => 'Price', 'comments'=>'Comments', 'status' =>'Status', 'relationship'=>'relationship']);
        $table->addColumn('title', 'Title', function($model){
            $model->id = $model->item_id;
            return ucwords($model->getTitle());
        });

        $table->setView('user.edit-user-table');
        $edit->enquired = $table;
        $edit->id = $edit->getRouteKey();
        return view('admin.detail-edit', compact('edit'));
    }

    public function enquired($id)
    {
        $user = User::find($id);
        $l = $user->enquired;
        $enquired = [];
            foreach($l as $enquire){
                $enquire->title;
                array_push($enquired, $enquire);
            }
        $user->id = $user->getRouteKey();
        return view('user.enquired', compact('user','enquired'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->update($request->all());

        $user->addToIndex();
        Flash::message('User updated');
        return redirect('home');
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $listed = $user->listed->count();
        $enquired = $user->enquired->count();

        if($listed == 0 && $enquired == 0){
            $client = ClientBuilder::create()->build();
            // make sure we pick up the index dynamically rather than statically
            $index = Config::get('elasticquent.default_index');
            $params = ['index'=>$index, 'type'=>'users', 'id'=>$user->id];
            $client->delete($params);
            $user->delete();
            Flash::message('User deleted');
        }
        elseif($listed > 0 || $enquired > 0) {
            Flash::message('User was not deleted as they have active items');
        }
        else{
             Flash::message('Error');
        }
        return redirect()->back();
     }

    
}
