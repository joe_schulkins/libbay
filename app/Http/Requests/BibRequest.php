<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BibRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'title' => 'required',
            'author' => 'required',
            'isbn' => 'required',
            'pubdate' => 'required|date'
        ];
    }

    /*
     * Return custom validation messages
     *
     * @return array
     */
    
    public function messages()
    {
        return [
            'title.required' => 'The title of the book is required.',
            'author.required' => 'The author of the book is required.',
            'isbn.required' => 'The isbn of the book is required.',
            'pubdate.required' => 'The published date of the book is required.',
            'pubdate.date' => 'The published date needs to be in numbers e.g. 2007.'
        ];
    }


}
