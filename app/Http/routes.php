<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/**
 * decode the hashed ids 
 */
Route::bind('uid', function($value, $route)
{
    return \Hashids::decode($value)[0];
});

// main page - displays search box
Route::get('/',	'PagesController@search');

// contact page
Route::get('contact', ['as'=>'contact','uses'=>'PagesController@contact']);

// cancel 
Route::get('cancel', ['as'=>'cancel','uses'=>'PagesController@cancel']);

// about page
Route::get('about', ['as'=>'about','uses'=>'PagesController@about']);

// address handling
Route::get('address', ['as'=>'address','uses'=>'AddressController@add']);

// reading list page given reading list id
Route::get('title/readinglist/{reading}', ['as'=>'readingList','uses'=>'TitleDataController@reading']);

// search the current database for existing ISBN - if not query copac for title
Route::post('title/search', ['as'=>'findIsbn','uses'=>'TitleDataController@findIsbn']);

// see if the title to be added already exists (uses all the isbn information retrieved by copac)
Route::post('title/add_title_item', ['as'=>'addTitleItem','uses'=>'TitleDataController@addTitleItem']);

// see if the title to be added already exists (uses all the isbn information retrieved by copac)
Route::post('title/add_item', ['as'=>'addNewItem','uses'=>'TitleDataController@addNewItem']);

// create a title manually
Route::get('title/createManual', ['as'=>'createManualTitle','uses'=>'TitleDataController@createManualTitle']);

/**
 * Resource for handling default routing queries
 */
Route::resource('title', 'TitleDataController' );

// show a title
Route::get('title/{id}', ['as'=>'showTitle','uses'=>'TitleDataController@show']);

/**
 * Resource for handling default routing queries
 */
Route::resource('isbn', 'IsbnDataController' );

/**
 * Edit the specified resource given its id
 */
Route::get('author/edit/{author}', ['as'=>'authorEdit', 'uses' => 'AuthorDataController@edit'] );
Route::get('title/edit/{title}', ['as'=>'titleEdit', 'uses' => 'TitleDataController@edit'] );
Route::get('item/edit/{item}', ['as'=>'itemEdit', 'uses' => 'ItemDataController@edit'] );
Route::get('isbn/edit/{isbn}', ['as'=>'isbnEdit', 'uses' => 'IsbnDataController@edit'] );
Route::get('user/edit/{uid}', ['as'=>'userEdit', 'uses' => 'UserDataController@edit'] );

/**
 * Update 
 */
Route::patch('title/update/{title}', ['as'=>'updateBib', 'uses' => 'UpdateBibDataController@update']);
Route::patch('user/update/{uid}', ['as'=>'userUpdate', 'uses' => 'UserDataController@update']);
Route::patch('user/item-update/{item}', ['as'=>'itemUpdate', 'uses' => 'ItemDataController@update']);

// Add fields to a form

// the page handling search queries - these are routed to elasticsearch
Route::get('listed/search', ['as'=>'search', 'uses' => 'TitleSearchController@index']);
	
// hide an item from view
Route::get('item/hide/{item}', ['as'=>'itemHide','uses'=>'ItemDataController@hide']);

// send a message about an item
Route::get('item/message/{item}', ['as'=>'itemMessage','uses'=>'ItemDataController@message']);

// arrange collection of item
Route::get('item/collect/{item}', ['as'=>'itemCollect','uses'=>'ItemDataController@collectItem']);

// arrange collection of item
Route::post('item/collect/{item}', ['as'=>'setCollectAttr','uses'=>'ItemDataController@setCollectAttr']);

// the item has been collected
Route::get('item/collected/{item}', ['as'=>'itemCollected','uses'=>'AdminUserDataController@itemCollected']);

// remove an item from listing
Route::get('item/remove/{item}', ['as'=>'itemDestroy','uses'=>'ItemDataController@destroy']);

// send an enquiry about an item (i.e. approach to buy)
//Route::get('item/enquire/{id}', ['as'=>'itemEnquire','uses'=>'ItemDataController@enquire']);

// add item to cart
Route::get('item/enquire/{item}', ['as'=>'itemEnquire','uses'=>'CartController@addToCart']);

// view the cart contents
Route::get('item/viewCart', ['as'=>'viewCart','uses'=>'CartController@viewCart']);

// cart checkout
Route::get('item/checkoutCart', ['as'=>'checkoutCart','uses'=>'CartController@checkoutCart']);

// cart remove item
Route::get('item/removeFromCart/{item}', ['as'=>'removeFromCart','uses'=>'CartController@removeFromCart']);

// item printSlip
Route::get('item/printSlip/{item}', ['as'=>'printSlip','uses'=>'ItemDataController@printSlip']);

// cancel the purchase of an item
Route::get('item/cancel/{item}/{uid}', ['as' =>'cancelItem', 'uses'=>'ItemDataController@cancelItem']);

// handles display of users enquired about an item to allow for accepting
Route::get('item/enquiries/{item}', ['as'=>'itemEnquiries','uses'=>'ItemDataController@enquiries']);

// controllers for authentication
Route::controllers([
	'auth' =>'Auth\AuthController',
	'password' => 'Auth\PasswordController'
	]);

/**
 * Get the user dashboard
 */
Route::resource('home', 'UserDataController');

// items user has listed
Route::get('home/listed/{uid}', ['middleware' => 'auth','as'=>'listed', 'uses'=>'UserDataController@listed']);

// items user has purchased
Route::get('home/enquired/{uid}', ['middleware' => 'auth','as'=>'enquired', 'uses'=>'UserDataController@enquired']);

/**
 * Get all notifications for current user
 */
Route::get('home/notifications/{uid}', ['middleware' => 'auth','as'=>'allNotifications', 'uses'=>'NotifyController@allNotifications']);

/**
 * Get current notifications for current user
 */
Route::get('home/notifications/display/{id}', ['middleware' => 'auth','as'=>'displayNotification', 'uses'=>'NotifyController@displayNotification']);


/**
 * Set the admin functions
 */
// list of new items
Route::get('admin/new_users', ['middleware' => 'isadmin','as'=>'adminNewUsers', 'uses'=>'AdminUserDataController@newUsers']);

// approve new users
Route::get('admin/approve/{uid}', ['middleware' => 'isadmin','as'=>'adminApproveUser', 'uses'=>'AdminUserDataController@approve']);

// edit records
Route::get('admin/edit/{admin}/{type}', ['middleware' => 'isadmin','as'=>'adminEdit', 'uses'=>'AdminUserDataController@edit']);

// edit records
Route::get('admin/edit/{uid}/user', ['middleware' => 'isadmin','as'=>'adminEditUser', 'uses'=>'AdminUserDataController@edit']);

// delete records
Route::get('item/destroy/{item}', ['as'=>'itemDelete', 'uses'=>'ItemDataController@destroy']);

// ban users
Route::get('admin/ban/{uid}', ['middleware' => 'isadmin','as'=>'adminBanUser', 'uses'=>'AdminUserDataController@ban']);

// make admin
Route::get('admin/make_admin/{uid}', ['middleware' => 'isadmin','as'=>'adminMakeAdminUser', 'uses'=>'AdminUserDataController@makeAdmin']);

// list of new items
Route::get('admin/new_items', ['middleware' => 'isadmin','as'=>'adminNewItems', 'uses'=>'AdminUserDataController@newItems']);

// route items to appropriate destroy methods
Route::get('admin/destroy/{id}/{type}', ['middleware' => 'isadmin','as'=>'adminDelete', 'uses'=>'AdminUserDataController@destroy']);

Route::get('admin/userDestroy/{uid}', ['middleware' => 'isadmin','as'=>'adminUserDelete', 'uses'=>'AdminUserDataController@userDestroy']);

// approve item
Route::get('admin/approve_item/{admin}', ['middleware' => 'isadmin','as'=>'adminItemApprove', 'uses'=>'AdminUserDataController@itemApprove']);

// send a message to the user
Route::get('admin/user_message/{admin}', ['middleware' => 'isadmin','as'=>'adminUserMessage', 'uses'=>'AdminUserDataController@userMessage']);

// list items for collection
Route::get('admin/adminCollect', ['middleware' => 'isadmin','as'=>'collectItem', 'uses'=>'AdminUserDataController@collectItem']);

Route::get('admin/clearShelf', ['middleware' => 'isadmin','as'=>'displayClearShelf', 'uses'=>'AdminUserDataController@displayClearShelf']);

Route::get('admin/clearShelf/{item}', ['middleware' => 'isadmin','as'=>'clearShelf', 'uses'=>'AdminUserDataController@clearShelf']);

// search for items, titles, users
Route::get('admin/search', ['middleware' => 'isadmin','as'=>'adminSearch', 'uses'=>'AdminSearchController@adminSearch']);

Route::get('admin/file-editor', ['middleware' => 'isadmin','as'=>'editText', 'uses'=>'PagesController@editText']);
Route::post('admin/file-editor', ['middleware' => 'isadmin','as'=>'editThisText', 'uses'=>'PagesController@editThisText']);
Route::post('admin/file-save', ['middleware' => 'isadmin','as'=>'saveText', 'uses'=>'PagesController@saveText']);

Route::resource('admin', 'AdminUserDataController');

Route::get('user/deleteMsg/{msg}', ['as'=>'deleteMsg', 'uses'=>'NotifyController@deleteMsg']);



