<?php

namespace App\Listeners;

use App\Events\ItemPurchaseCancelled;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Notifynder;
use Mail;
use Config;

class ItemPurchaseHasBeenCancelled
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ItemPurchaseCancelled  $event
     * @return void
     */
    public function handle(ItemPurchaseCancelled $event)
    {
        $user = $event->item->listed()->first();
        $item = $event->item;
        $title = $event->item->title()->first()->title;
        $ref = $event->item->ref;

        $enq =  $item->enquired()->first();
        $item->enquired()->detach($enq);
        $item->status = 'listed';
        $item->disp = 'show';
        $item->save();

        /**
        Notifynder::category('sale_cancelled')
        ->from(1)
        ->to($user->id)
        ->url('')
        ->extra(compact('title', 'ref'))
        ->send();
        **/

        $data['recip'] = 'seller';
        Mail::send(
            'emails.sale-cancelled', ['data'=>$data, 'user'=>$user, 'item'=>$item], function($m) use ($user, $item){
                    $m->from(Config::get('bosbooks.admin_email'), Config::get('bosbooks.name'));
                    $m->to($user->email)->subject('Sale Cancelled');
                 });

        return;

    }
}
