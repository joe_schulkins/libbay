<?php

namespace App\Listeners;

use App\Events\ItemCollected;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendItemCollectedEmail
{
    
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ItemCollected  $event
     * @return void
     */
    public function handle(ItemCollected $event)
    {
       
    }
}
