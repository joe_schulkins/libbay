<?php

namespace App\Listeners;

use App\Events\TitleCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Livbay\Rlist\Rlist as Rlist;

class TitleHasBeenCreated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Once the title is created, look for reading lists
     *
     * @param  TitleCreated  $event
     * @return void
     */
    public function handle(TitleCreated $event)
    {
       $rlist = collect();
       $event->title->isbns->each(function($isbn) use ($rlist){
            $r = new Rlist;
            $r->getRlists($isbn->isbn, "isbn");
            $rlist->push($r);
        }); 
       return $rlist;
    }
}
