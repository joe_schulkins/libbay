<?php

namespace App\Listeners;

use App\Events\UserApproved;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;
use Config;

class UserHasBeenApproved
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserApproved  $event
     * @return void
     */
    public function handle(UserApproved $event)
    {
        $user = $event->user;
        Mail::send(
            'emails.user-approved', ['user'=>$user], function($m) use ($user){
                    $m->from(Config::get('bosbooks.admin_email'), Config::get('bosbooks.name'));
                    $m->to($user->email)->subject('Account Approved');
                 });

        return;
    }
}
