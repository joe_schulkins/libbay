<?php

namespace Livbay\Address;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'addresses';
	// fillable fields that can be mass assigned when using Eloquent
	protected $fillable = [
		'field1',
		'field2',
		'field3',
		'field4',
		'postcode',
		'phone',
		'oth_email'
	];
    //
}
