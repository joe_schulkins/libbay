<?php

namespace Livbay\Author;

use Illuminate\Database\Eloquent\Model;
use Elasticquent\ElasticquentTrait;
use Gbrock\Table\Traits\Sortable;

class Author extends Model
{
    use ElasticquentTrait, Sortable;
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'authors';

    protected $fillable = [
		'lname',
		'fname'
	];

    /**
     * The attributes which may be used for sorting dynamically.
     *
     * @var array
     */
    protected $sortable = ['lname', 'fname', 'created_at'];

    /**
     * elasticsearch mappings (field datatypes)
     * @var array database field and elasticsearch settings 
     */
    protected $mappingProperties = array(
            'lname' => array(
                'type' => 'string',
                'analyzer' => 'standard'
            ),
            'fname' => array(
                'type' => 'string',
                'analyzer' => 'standard'
            ),
            'type' => array(
                'type' => 'string',
                'analyzer' => 'standard'
            )
        );
    
    public function titles(){
        return $this->belongsToMany('Livbay\Title\Title')->withTimestamps();   
    }

    public function addAuthorToTitle($data, $tid){
        $author = Author::firstOrCreate([
            'lname' => trim(mb_strtolower((string)$data['lname'], 'UTF-8')),
            'fname' => trim(mb_strtolower((string)$data['fname'], 'UTF-8'))
            ]);
        $author->titles()->attach([$tid]);
        $author->addToIndex();
        return $author;
    }
}
