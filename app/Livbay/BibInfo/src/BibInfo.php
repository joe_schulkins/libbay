<?php
/**
 * BibInfo
 *
 * @author Joe Schulkins <j_schulkins@hotmail.com>
 * @version 0.1.0
 * @package CollectsMetadata
*/

namespace BibInfo;


abstract class BibInfo{

	public $isbn;
	public $bib_details = [];

	/**
	 * We need an ISBN for searching
	 * @param [type] $isbn [description]
	 */
	public function __construct($isbn){
		$this->isbn = $isbn;
	}

	/**
	 * If not using SRU, format the endpoint url with the ISBN in the correct location
	 * setUrl replace {isbn} token in url with isbn
	 * @param string isbn 
	 * @param string url with {isbn} token - 'http://copac.jisc.ac.uk/search?isn={isbn}&format=XML+-+MODS'
	 * @return string url with isbn inserted
	 */
	public static function setUrl($isbn, $url){
		$url = str_replace('{isbn}', $isbn, $url);
		return $url;
	}

	/**
	 * setup CURL to perform a request to the url given in setUrl
	 * @param  string $url url endpoint
	 * @return mixed      data returned by endpoint: marcxml, mods etc.
	 */
	public static function useCurl($url){
		$curl = curl_init();
		curl_setopt_array($curl, array(
    		CURLOPT_RETURNTRANSFER => 1,
    		CURLOPT_URL => $url,
    		CURLOPT_ENCODING => "gzip",
    		CURLOPT_FOLLOWLOCATION => true
		));

		$curl_result = curl_exec($curl);
		curl_close($curl);
		return $curl_result;
	}

	/**
	 * get the value of a subfield given the $att value of an $object['tag']
	 * @param  obj $object simplexmlelement object
	 * @param  str $att    va;ue of the 'tag' attribute
	 * @return str         data in subfield of object
	 */
	public function xml_attribute($object, $att){
    			if(null !== ($object["tag"]== $att))
        			return (string) $object->subfield;
			}

}
