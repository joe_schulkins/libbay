<?php
/**
 * CollectsMetadata
 *
 * @author Joe Schulkins <j_schulkins@hotmail.com>
 * @version 0.1.0
 * @package CollectsMetadata
*/

namespace BibInfo\CollectsMetadata;


interface CollectsMetadata{
	public function retrieval();
}