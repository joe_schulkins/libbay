<?php
/**
 * COPAC
 *
 * @author Joe Schulkins <j_schulkins@hotmail.com>
 * @version 0.1.0
 * @package CollectsMetadata
*/
namespace BibInfo;

use BibInfo\BibInfo as BibInfo;
use Scriptotek\Sru\Client as SruClient;
use Livbay\Isbn\Isbn;
use Carbon\Carbon;

/**
* Copac class
*
* @return  array [<description>]
*/
class Copac extends BibInfo{
	
	/**
	 * Set the SRU client and search
	 * once finished execute retrieval() method
	 */
	function __construct($isbn){
		$this->isbn = $isbn;
		$url = "http://copac.ac.uk:3000/copac";
        $client = new SruClient($url, array('schema'=>'mods','version'=>'1.1'));
        $this->response = $client->first("bath.isbn='".$isbn."'");

      
        $this->retrieval();
	}
	
	public function retrieval(){

       		$record = $this->response;

       		if($record != null){
       		//	var_dump($record);
        	$bib_record = [];
        	$bib_record['position'] = trim($record->position);
        	$bib_record['title'] = trim(mb_strtolower((string)$record->data->el->mods->titleInfo->title, 'UTF-8'));
        	$bib_record['subtitle'] = trim(mb_strtolower((string)$record->data->el->mods->titleInfo->subTitle, 'UTF-8'), '[]');
        	$bib_author = [];
        	
        	foreach( $record->data->el->mods->name as $name){
				$au_split = explode(",", (string)$name->namePart, 2);
				if(!isset($au_split[1])){
					return false;
				}
				elseif(sizeof($au_split) > 0){
                	$bib_author['lname'] = trim(mb_strtolower($au_split[0], 'UTF-8'));
                	$bib_author['fname'] = trim(mb_strtolower($au_split[1], 'UTF-8'));
                	$au[] = $bib_author;
            	}
			}
			if(isset($au)){
				$bib_record['authors']= $au;
			}

			$origin = [];
			foreach ($record->data->el->mods->originInfo as $key => $v) {
				foreach($v as $key => $value ){
					$origin[$key]= $v->$key;
					if ($key == 'edition'){
						$bib_record['edition'] = trim((string) $origin['edition']);
					}
					if($key == 'dateIssued'){
						$date = preg_replace("/[^0-9]/", "", (string)$origin['dateIssued']);
						preg_match("/[0-9]{4}/",$date,$matches);
						$bib_record['pubdate'] = $matches[0];
					}

				}
			}

        	$bib_isbn = [];
			foreach ($record->data->el->mods->identifier as $bib_key) {
				if($bib_key['type'] == 'isbn'){
					//$ib_dash = str_replace('-', '', (string)$bib_key);
					$ib_reg = preg_replace("/[^0-9xX].+/", "",(string)$bib_key);
					$isbn13 = Isbn::isbn13(trim($ib_reg));
					$isbn = new \ISBN\Isbn();
					$isbn->hyphens->removeHyphens($isbn13);
					if(strlen($isbn13) < '13' ){
						$isbn13 = $isbn->translate->to13($isbn13);
						$isbn13 = (string)$isbn13;
					}
					$bib_isbn[] = $isbn13;
				}
			}

			$bib_record['isbns'] = array_unique($bib_isbn);
			$array_pos = $bib_record['position']-1; 
			
        $this->bib_details = $bib_record;
        
		return $this->bib_details;
		}
		else{
			$bib_record['isbns'] = false;
			$this->bib_details = $bib_record;
			return $this->bib_details;
		}
	}

}