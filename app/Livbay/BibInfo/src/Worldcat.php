<?php
/**
 * COPAC
 *
 * @author Joe Schulkins <j_schulkins@hotmail.com>
 * @version 0.1.0
 * @package CollectsMetadata
*/
namespace BibInfo;

use BibInfo\BibInfo as BibInfo;
use Scriptotek\Sru\Client as SruClient;
use Livbay\Isbn\Isbn;
use Carbon\Carbon;

/**
* Copac class
*
* @return  array [<description>]
*/
class Worldcat extends BibInfo{
	
	/**
	 * Set the SRU client and search
	 * once finished execute retrieval() method
	 */
	function __construct($isbn){
		$this->isbn = $isbn;

		$url = 'http://www.worldcat.org/webservices/catalog/search/worldcat';

		//need to hide wskey
		$client = new SruClient($url);
        $this->response = $client->search("srw.bn=".$isbn, 1, 1, ['wskey'=>'8Ca1PvPch5v3IM6QDfLSIapAaYwQ6j6pdsOeIAczCAN4wPOk4meWvIewUPDXsvfmsSS9gUrxGdf16QRn']);
        Worldcat::retrieval();
	}
	
	public function retrieval(){

       		$record = $this->response;

       		if($record != null){

        	$bib_record = [];
        	
        	$datafields = $record->records[0]->data->el->record->children();
        	$bib_isbn = [];
        	foreach ($datafields->datafield as $obj) {
        		$bib_author = [];
        		
        		
        		foreach($obj->attributes() as $a => $b){
        			// title tag with statement of responsibility
        			if((string)$a == 'tag' &&  (string)$b == '245'){
        				$re = "/(?<![.+]):/";
        				$rp =  "/(?<![.+])\//";
        				$str = (string)$obj->subfield[0];
        				$str1 = (string)$obj->subfield[1];
        				$ti = preg_replace($re,"",$str);
        				$bib_record['title'] = trim(mb_strtolower($ti, 'UTF-8'));
        				if(preg_match($re, $str, $matches)){
        					$str1 = preg_replace($rp,"",$str1);
        					$bib_record['subtitle'] = trim(mb_strtolower($str1, 'UTF-8'));
        				}
        			}

        			// authors
        			if((string)$a == 'tag' &&  (string)$b == '100'){
        				$str = (string)$obj->subfield[0];
        				$au_split = explode(",", $str, 2);
						if(!isset($au_split[1])){
							return false;
						}
						elseif(sizeof($au_split) > 0){
                			$bib_author['lname'] = trim(mb_strtolower($au_split[0], 'UTF-8'));
                			$bib_author['fname'] = trim(mb_strtolower($au_split[1], 'UTF-8'));
                			$au[] = $bib_author;
            			}
        			}

        			// editors or compilers etc.
        			if((string)$a == 'tag' &&  (string)$b == '700'){
        				$str = (string)$obj->subfield[0];
        				$au_split = explode(",", $str, 2);
						if(!isset($au_split[1])){
							return false;
						}
						elseif(sizeof($au_split) > 0){
                			$bib_author['lname'] = trim(mb_strtolower($au_split[0], 'UTF-8'));
                			$bib_author['fname'] = trim(mb_strtolower($au_split[1], 'UTF-8'));
                			$au[] = $bib_author;
            			}
        			}

        			// isbn data : get all isbns, convert them to isbn13
        			if((string)$a == 'tag' &&  (string)$b == '020'){
					
						$ib_reg = preg_replace("/[^0-9xX].+/", "",(string)$obj->subfield[0]);
						$isbn13 = Isbn::isbn13(trim($ib_reg));
						$isbn = new \ISBN\Isbn();
						$isbn->hyphens->removeHyphens($isbn13);
						if(strlen($isbn13) < '13' ){
							$isbn13 = $isbn->translate->to13($isbn13);
							$isbn13 = (string)$isbn13;
						}
						array_push($bib_isbn,$isbn13);
						
					}

					// edition statement
					if((string)$a == 'tag' &&  (string)$b == '250'){
						$bib_record['edition']  = (string)$obj->subfield[0];	
					}  

					// pubdate statement
					if((string)$a == 'tag' &&  (string)$b == '260'){
						$re = "/(?<![.+])\./";
						foreach($obj->subfield as $val){
							if(preg_match($re, $val, $matches)){
								preg_match("/[0-9]{4}/",$val,$mat);
								$bib_record['pubdate'] = preg_replace($re,"",$mat[0]);
							}
						}	
					}     
        			
					
        		}

        		$unq = array_unique($bib_isbn);
           		$bib_record['isbns'] = array_values($unq); 

				if(isset($au)){
					$bib_record['authors']= $au;
				}

        		
        		
        	}
        	$this->bib_details = $bib_record;
        	return $this->bib_details;
        }
        else{
			$bib_record['isbns'] = false;
			$this->bib_details = $bib_record;
			return $this->bib_details;
		}
	}

}