<?php

namespace Livbay\Isbn;

use Illuminate\Database\Eloquent\Model;
use Elasticquent\ElasticquentTrait;

class Isbn extends Model
{

  use ElasticquentTrait;
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'isbns';
    
  /** 
   * fillable fields that can be mass assigned when using Eloquent
   * @var 
   */
	protected $fillable = [
		'isbn'
	];
      /**
     * elasticsearch mappings (field datatypes)
     * @var array database field and elasticsearch settings 
     */
     protected $mappingProperties = array(
            'isbn' => array(
                'type' => 'string',
                'analyzer' => 'standard'
            ),
            'type' => array(
                'type' => 'string',
                'analyzer' => 'standard'
            )
        );
    
    public function titles(){
        return $this->belongsToMany('Livbay\Title\Title')->withTimestamps();   
    }

    public function rlists(){
        return $this->belongsToMany('Livbay\Rlist\Rlist')->withTimestamps();   
    }

    public static function isbn13($isbns){
    if(is_array($isbns)){
        $arr_isbn = [];
        
        foreach ($isbns as $key => $value) {
          $isbn = new \Isbn\Isbn();
          if($isbn->check->is13($value) == true){
            $arr_isbn[$key] = $value;
          }
          else{
            $value = $isbn->translate->to13($value);
            $arr_isbn[$key] = $value;
          }
        }
        return array_unique($arr_isbn);
      }

      else{
        $isbn = new \Isbn\Isbn();
        if($isbn->check->is13($isbns) == true){
          return $isbns;
        }
        else{
          return $isbn->translate->to13($isbns);
        }
      }
    }

    public function addIsbnToTitle($data, $tid){
          $ib_reg = preg_replace("/[^0-9xX].+/", "",(string)$data);
          $isbn13 = Isbn::isbn13(trim($ib_reg));
          $isbn = new \Isbn\Isbn();
          $isbn->hyphens->removeHyphens($isbn13);
          if(strlen($isbn13) < '13' ){
              $isbn13 = $isbn->translate->to13($isbn13);
              $isbn13 = (string)$isbn13;
           }
          $isn = $isbn13;
          $isbn = Isbn::firstOrCreate([
              'isbn'=> $isn
              ]);
          $isbn->titles()->attach($tid);
          $isbn->addtoIndex();
          return $isbn;
    }
}
