<?php

namespace Livbay\Item;
use Livbay\Title\Title as Title;
use Livbay\Isbn\Isbn as Isbn;
use Livbay\Author\Author as Author;



class AddTitleItem{

	public $title;
	public $userID;
	public $titleID;

	
	public function __construct($isbn, $user){
        // need an isbn or the lookup will fail - just assign a default one
        if($isbn == null || ''){
            $isbn = '0000000000';
        }

        if($user != null){
            $userID = $user;
        }
		elseif(\Auth::user() !== null){
			$userID = \Auth::user()->id;
		}
		else{
			$userID = null;
		}

        // get the isbn string if an object or array
        if(gettype($isbn) == 'object' ){
            $isbn = $isbn->first()->isbn;
        }
        elseif(gettype($isbn) == 'array'){
            $isbn = $isbn[0];
        }
        else{
            $isbn = $isbn;
        }
       

        $indb = \Livbay\Isbn\Isbn::where('isbn', '=', $isbn)->first();
        
        if( $indb !== null ){
            $title = $indb->titles->first();
            $this->titleID = $indb->titles->first()->id;
            $exists = true;
        }
        if( $indb == null ){
            $title = null;
            $exists = false;
        }
        $this->exists = $exists;
        $this->title = $title;
        $this->userID = $userID;
        echo "Processing ISBN: ".$isbn."\r\n";
        return $this;
	}

    /**
     * get information from our endpoint
     * @param  int $isbn isbn to searhc for
     * @return obj       title information
     */
    public function searchForTitle($isbn){
        $title = new \BibInfo\Copac($isbn);
        $this->title = $title->bib_details;
        return $this->title;
    }

    public function updateTitle($data){
        $this->title = $data;
        return $this->title;
    }

    /**
     * create a new title and once created, call reading lists and item methods
     * @param request $request information about the title
     */
    public function addTitle($request){
  //      dd($this);
        $f = new \Livbay\Title\Title;
        $f = $f->createBib($this->title);
       
        //var_dump($f[0]->authors);
        if($f[0] != (NULL || false)){

            $this->titleID = $f[0]->id;
            echo "Title created\r\n";
       	   // $rlist = $this->addRlists($this->title);
            echo "Reading Lists searched\r\n";
            $item = $this->addItem($request, $this->userID, $this->titleID);
            echo "Items added\r\n";
        }
        else{
            return;
        }
    }

    /**
     * get reading lists for a title using isbns using the getRlists method
     * @param array $title containing all title information including isbns
     */
    public function addRlists($title){
        echo "Searching for reading lists...\r\n";

       $rlist = collect();
       $event->title->isbns->each(function($isbn) use ($rlist){
            $r = new Rlist;
            $r->getRlists($isbn->isbn, "isbn");
            $rlist->push($r);
        }); 

       if($rlist->count() > 0){
            echo $rlist->count()." Reading Lists found.\r\n";
       }

       return $rlist; 
    }

    /**
     * add item to a title
     * @param object $request item description
     * @param int $userID  id of User listing item
     * @param int/obj $titleID integer or object containing the title the item belongs to
     */
    public function addItem($request, $userID, $titleID){

        echo "Adding items...\r\n";
        if(is_int($titleID) == false){
            $titleID = $titleID->id;
        }
        $item = new \Livbay\Item\Item;
        $item->addItem($request,$userID,$titleID); 
        }
}