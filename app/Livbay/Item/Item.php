<?php

namespace Livbay\Item;

use Livbay\User\User as User;
use Livbay\Title\Title as Title;
use Illuminate\Database\Eloquent\Model;
use Gbrock\Table\Traits\Sortable;
use Fenos\Notifynder\Notifable;
use Elasticquent\ElasticquentTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model
{
  use Sortable,Notifable,ElasticquentTrait, SoftDeletes;
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'items';

    protected $fillable = ['price', 'condition', 'comments', 'status', 'disp', 'location', 'collect'];

    /**
     * The attributes which may be used for sorting dynamically.
     *
     * @var array
     */
    protected $sortable = ['price', 'condition', 'created_at', 'ref', 'collect'];

    protected $dates = ['collect' , 'deleted_at', 'cleared_at'];

      /**
     * elasticsearch mappings (field datatypes)
     * @var array database field and elasticsearch settings 
     */
    protected $mappingProperties = array(
            'ref' => array(
                'type' => 'string',
                'analyzer' => 'standard'
            )
        );
    
   	/**
   	 * An item belongs to a title
   	 *
   	 * @return  array
   	 * usage where $example is the item:
   	 * $example->user->toArray();
   	 */
   	public function title(){
   		return $this->belongsToMany('Livbay\Title\Title')->withTimestamps();
   	}

    /**
     * An item is listed by a user
     * @return array of items that match 'listed'
     */
    public function listed(){
        return $this->belongsToMany('Livbay\User\User')->withTimestamps()->wherePivot('relationship', 'listed');
    }

    /**
     * An item can be enquired by a user
     * @return array items that match 'enquired'
     */
    public function enquired(){
        return $this->belongsToMany('Livbay\User\User')->withTimestamps()->wherePivot('relationship', 'enquired');
    }

    public function addItem($request,$userID,$titleID ){
      
      $title = Title::find($titleID);
      $user = User::find($userID);

      if(null == $request->status){
        $request->status = 'review';
      }
      if(null == $request->disp){
        $request->disp = 'hide';
      }

      $re = "/[£|p]/";
        $price = $request->price;
        if(preg_match($re, $price)){
            $price = preg_replace($re, '', $price);
        }

      $item = Item::create([
            'condition' => $request->condition,
            'comments' => $request->comments,
            'price' => $price,
            'status' => $request->status,
            'disp' => $request->disp,
            'location' => $request->location
        ]);

      /**
       * lets generate a random number to act as a reference and check its not been used already
       */
      do {
        $rand = mt_rand();
      } while ( Item::where('ref','=', $rand)->count() != 0);

      $item->ref = $rand;
      

      $item->title()->attach($title->id);
      $item->listed()->attach([$user->id => ['relationship'=>'listed']]);
      $item->save();
      $item->addtoIndex();
      return $item; 
    }

    public function getTitle(){
      return $this->title->first()->title;
    }

}
