<?php

namespace Livbay\Rlist;

use Illuminate\Database\Eloquent\Model;
use Livbay\Isbn\Isbn as Isbn;
use Gbrock\Table\Traits\Sortable;
use Elasticsearch;
use Elasticsearch\ClientBuilder;
use Elasticquent\ElasticquentTrait;

class Rlist extends Model
{
use Sortable, ElasticquentTrait;
    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'rlists';

    /**
     * what can be mass assigned
     * @var array
     */
  	protected $fillable = [
  		'rlist_url',
  		'rlist_name'
  	];

    /**
     * The attributes which may be used for sorting dynamically.
     *
     * @var array
     */
    protected $sortable = ['rlist_name', 'created_at'];

    /**
     * elasticsearch mappings (field datatypes)
     * @var array database field and elasticsearch settings 
     */
    protected $mappingProperties = array(
            'rlist_name' => array(
                'type' => 'string',
                'analyzer' => 'standard'
            ),
            'type' => array(
                'type' => 'string',
                'analyzer' => 'standard'
            )
        );

    /**
     * Pivot table for isbns_rlists
     * @return pivot table
     */
    public function isbns(){
        return $this->belongsToMany('Livbay\Isbn\Isbn')->withTimestamps();   
    }

    public function titles(){
      $isbns = $this->isbns;
      $bk = collect();
      foreach($isbns as $isbn){
        $bk->push($isbn->titles->first());
      }
      $this->book = $bk->unique('title');
      $this->bk_count = count($this->book);
      return $this;
    }

     /**
     * use Talis Reading List API to get reading list information
     * @param  array or variable $con control data e.g. isbns, issn, doi
     * @param  string $type  what type of data is being sent through (isbn, issn, doi, lcn)
     * @return array        reading list url and title
     */
    public function getRlists($con, $type){

      $url = 'http://readinglists.liverpool.ac.uk/'.$type.'/'.$con.'/lists.json';
      $curl = curl_init();
      curl_setopt_array($curl, array(
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $url,
        CURLOPT_ENCODING => "gzip",
        CURLOPT_FOLLOWLOCATION => true
      ));
      $result = curl_exec($curl);
      $resultStatus = curl_getinfo($curl);
      curl_close($curl);

      if($resultStatus['http_code'] == 200){
        $json = json_decode($result,true);        
        $lists = [];
          foreach($json as $key => $value){
            $list = [];
            $rlist = Rlist::firstOrCreate([
                'rlist_url' => $key,
                'rlist_name' => $value
              ]);
            $isbn = Isbn::firstOrCreate([
                'isbn' => $con
              ]);           
            array_push($list, $rlist, $isbn);
            array_push($lists, $list); 
          }

          /**
           * when updating the pivot table in the above foreach loop
           * multiple duplicates were inserted as it looked
           * at first rlist and then isbn
           */
          foreach($lists as $detail){
            $rID = $detail[0]->id;
            $iID = $detail[1]->id;
            $detail[1]->rlists()->attach($rID);
          }
        return $list;
      }
      else{
        return 'no list';
      }

    }

    /**
     * Get the value of the model's route key.
     *
     * @return mixed
     */
    public function getRouteKey()
    {
       // dd($this->getKey());
        return Hashids::encode($this->getKey());
    }

    public function indexUser(){
        return $this->addToIndex();
    } 

}
