<?php

namespace Livbay\Title;


use Livbay\Isbn\Isbn;
use Livbay\Author\Author;
use Livbay\Rlist\Rlist as Rlist;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use BibInfo\Copac;
use Elasticquent\ElasticquentTrait;
use Gbrock\Table\Traits\Sortable;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Events\TitleCreated;

class Title extends Model
{	
    use ElasticquentTrait, Sortable, SoftDeletes;
     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'titles';
	// fillable fields that can be mass assigned when using Eloquent
	protected $fillable = [
		'title',
		'subtitle',
		'edition',
		'volume',
		'pubdate'
	];
    
    protected $dates = ['pubdate', 'deleted_at'];

    /**
     * The attributes which may be used for sorting dynamically.
     *
     * @var array
     */
    protected $sortable = ['title', 'pubdate', 'created_at'];

    /**
     * elasticsearch mappings (field datatypes)
     * @var array database field and elasticsearch settings 
     */
    protected $mappingProperties = array(
            'title' => array(
                'type' => 'string',
                'analyzer' => 'standard'
            ),
            'subtitle' => array(
                'type' => 'string',
                'analyzer' => 'standard'
            ),
            'pubdate' => array(
                'type' => 'date',
                'format'=> 'yyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis'
            ),
            'type' => array(
                'type' => 'string',
                'analyzer' => 'standard'
            )
        );

    /**
     * Set the date
     * @param $date 
     */
    public function setPubdateAttribute($date){
    	$this->attributes['pubdate'] = Carbon::create($date,1,1,1);
    }

    /**
     * [scopeBibRecord description]
     * @param  [type] $query [description]
     * @param  [type] $id    [description]
     * @return [type]        [description]
     */
    public function scopeBibRecord($query, $id){
        return $query->where('id', '$id');
    }

    /**
     * Isbn pivot table relationship isbn_title
     * @return pivot table [description]
     */
    public function isbns(){
        return $this->belongsToMany('Livbay\Isbn\Isbn')->withTimestamps();   
    }

    /**
     * Authors pivot table relationship author_title
     * @return pivot table [description]
     */
    public function authors(){
        return $this->belongsToMany('Livbay\Author\Author')->withTimestamps();   
    }

    /**
     * Items pivot table relationship item_title
     * @return pivot tabke [description]
     */
    public function items(){
        return $this->belongsToMany('Livbay\Item\Item')->withTimestamps();   
    }

    /**
     * filter out those items that have a display level of 'hide'
     * @return collection of items
     */
    public function showItems(){
           return $this->items->filter(function($item){
              return $item->disp != 'hide';
            });
    }

    /**
     * Get the value of the model's route key.
     *
     * @return mixed
     */
    public function getRouteKey()
    {
       // dd($this->getKey());
        return Hashids::encode($this->getKey());
    }

    public function createRlists(){

       $rlist = collect();
       $this->isbns->each(function($isbn) use ($rlist){
            $r = new Rlist;
            $r->getRlists($isbn->isbn, "isbn");
            $rlist->push($r);
        }); 

       if($rlist->count() > 0){
            echo $rlist->count()." Reading Lists found.\r\n";
       }

       return $this->isbns; 
    }

    public function removeRlists(){
        $this->isbns->each(function($isbn){
               // dd($isbn);
                $isbn->rlists()->detach($isbn->id);
                return $isbn;
            });
        return $this->isbns;
    }

    public function createBib($data){
        if(sizeof($data) == 0){
            return false;
        }
        if($data['isbns'] != false){

        if(isset($data['pubdate']) && $data['pubdate'] != ''){
            $date = preg_replace("/[^0-9]/", "", (string)$data['pubdate']);
            preg_match("/[0-9]{4}/",$date,$matches);
            $date = $matches[0];
        }
        else{
            $date = null;
        }
        

        $title = Title::firstOrCreate([
            'title'=>trim(mb_strtolower((string)$data['title'], 'UTF-8')),
            'subtitle'=>isset($data['subtitle'])?trim(mb_strtolower((string)$data['subtitle'], 'UTF-8')):'',
            'edition'=>isset($data['edition'])?$data['edition']:'',
            'volume'=>isset($data['volume'])?$data['volume']:'',
            'pubdate'=>$date
            ]);

        if(isset($data['authors'])){
            foreach($data['authors'] as $au){
                // should authors just be created?
                $author = Author::firstOrCreate([
                    'lname' => trim(mb_strtolower((string)$au['lname'], 'UTF-8')),
                    'fname' => trim(mb_strtolower((string)$au['fname'], 'UTF-8'))
                    ]);
                $author->titles()->attach([$title->id]);
                $author->addToIndex();
            }
        }

        if(is_array($data['isbns'])){
            $isbns = array_unique($data['isbns']);
        }
        else{
            $isbns = $data['isbns'];
        }

        foreach($isbns as $isn){
            $ib_reg = preg_replace("/[^0-9xX].+/", "",(string)$isn);
            $isbn13 = Isbn::isbn13(trim($ib_reg));
            $isbn = new \ISBN\Isbn();
            $isbn->hyphens->removeHyphens($isbn13);
            if(strlen($isbn13) < '13' ){
                $isbn13 = $isbn->translate->to13($isbn13);
                $isbn13 = (string)$isbn13;
            }
            $isn = $isbn13;
            $isbn = Isbn::firstOrCreate([
                'isbn'=> $isn
                ]);
            $isbn->titles()->attach($title->id);
            $isbn->addtoIndex(); 
        }
        $title->addToIndex();

        $title->isbns;
        \Event::fire(new TitleCreated($title));

        return [$title, $isbns] ;
    }
    else{
        return false;
    }

  }
}
