<?php

namespace Livbay\User;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Gbrock\Table\Traits\Sortable;
use Fenos\Notifynder\Notifable;
use Elasticquent\ElasticquentTrait;
use Vinkla\Hashids\Facades\Hashids;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use ElasticquentTrait, Authenticatable, CanResetPassword, Sortable, Notifable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['fname', 'lname','uni_id', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token', 'uni_id', 'administrator', 'valid'];

    /**
    * Attributes to be changed eg change integer on DB to boolean
    * @var array
    */
    protected $casts = ['admin'=>'boolean', 'valid'=>'boolean'];

    /**
     * The attributes which may be used for sorting dynamically.
     *
     * @var array
     */
    protected $sortable = ['lname', 'fname', 'email', 'created_at'];

    /**
     * elasticsearch mappings (field datatypes)
     * @var array database field and elasticsearch settings 
     */
    protected $mappingProperties = array(
            'lname' => array(
                'type' => 'string',
                'analyzer' => 'standard'
            ),
            'fname' => array(
                'type' => 'string',
                'analyzer' => 'standard'
            ),
            'email' => array(
                'type' => 'string',
                'format'=> 'standard'
            ),
            'uni_id' => array(
                'type' => 'string',
                'analyzer' => 'standard'
            ),
            'type' => array(
                'type' => 'string',
                'analyzer' => 'standard'
            )
        );

    /**
     * used by RedirectIfNotAdmin middleware
     * @return boolean
     */
    public function isNotAdmin(){
        $user = \Auth::user()->pluck('admin');
        if($user == true){
            return true;
        }
        else{
            return false;
        }
    }

    public function getAdminAttribute($value){
        return (bool) $value;
    }

    /**
     * An item is listed by a user
     * @return array of items that match 'listed'
     */
    public function listed(){
        return $this->belongsToMany('Livbay\Item\Item')->withTimestamps()->wherePivot('relationship', 'listed');
    }

    /**
     * An item can be enquired by a user
     * @return array items that match 'enquired'
     */
    public function enquired(){
        return $this->belongsToMany('Livbay\Item\Item')->withTimestamps()->wherePivot('relationship', 'enquired');
    }

    public function indexUser(){
        return $this->addToIndex();
    }

    /**
     * Get the value of the model's route key.
     *
     * @return mixed
     */
    public function getRouteKey()
    {
       // dd($this->getKey());
        return Hashids::encode($this->getKey());
    }
}
