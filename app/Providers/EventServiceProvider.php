<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\ItemCollected' => [
            'App\Listeners\SendItemCollectedEmail',
        ],
        'App\Events\TitleCreated' => [
            'App\Listeners\TitleHasBeenCreated',
        ],
        'App\Events\ItemPurchaseCancelled' => [
            'App\Listeners\ItemPurchaseHasBeenCancelled',
        ],
        'App\Events\UserApproved' => [
            'App\Listeners\UserHasBeenApproved',
        ],
        'auth.logout' => [
            'App\Listeners\UserHasLoggedOut',
        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //
    }
}
