<?php

return [
	'name' => env('BOS_NAME', 'BoS Book'),
	'domain' => env('BOS_DOMAIN', 'localhost'),
	'admin_email' => env('BOS_EMAIL','postmaster@localhost.com'),
	'daysToBuy' => env('BOS_DAYSTOBUY', 7)
];