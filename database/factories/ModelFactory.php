<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(Livbay\User\User::class, function (Faker\Generator $faker) {
    $email = $faker->safeEmail;
    return [
        'lname' => $faker->lastName,
        'fname' => $faker->firstName,
        'email' => $email,
        'uni_id' => $faker->unique()->numberBetween($min = 100000, $max = 900000000),
        'password' => bcrypt('password'),
        'pay' => $email,
        'remember_token' => str_random(10)
    ];
});

$factory->define(Livbay\Item\Item::class, function (Faker\Generator $faker) {

/**
 * turn off test data
 * @var [type]
 */
 /**   $price = $faker->randomElement($array = array ('0','1','2','5','10'));
    $condition = $faker->randomElement($array = array ('Very Good','Good','Acceptable'));
    $status =  $faker->randomElement($array = array('review','listed','purchased','collect'));
    $comments = $faker->text($maxNbChars = 200);

    if($status  == 'listed'){
        $disp = 'show';
    }
    else{
        $disp = 'hide';
    }

    if($status == 'review'){
        $location = null;
    }
    else{
        $location = 'sjl';
    }
**/
 /**   $item = [
        'price' => $price,
        'condition' => $condition,
        'status' => $status,
        'disp' => $disp,
        'location' =>$location,  
        'comments' => $comments
    ];
**/
    $item = [
        'price' => '0',
        'condition' => 'good',
        'status' => 'review',
        'disp' => 'hide',
        'location' => 'sjl',  
        'comments' => 'none'
    ];

    return $item;

});

