<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRlistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rlists', function (Blueprint $table) {
            $table->increments('id');
            $table->string('rlist_url')->nullable();
            $table->string('rlist_name');
            $table->string('type')->default('rlist');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rlists');
    }
}
