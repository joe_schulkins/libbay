<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Livbay\Title\Title as Title;
use Livbay\Isbn\Isbn as Isbn;
use Livbay\Item\Item as Item;
use Livbay\Author\Author as Author;
use Livbay\User\User as User;
use Livbay\Rlist\Rlist as Rlist;

use \Elasticquent\ElasticquentCollection as ElasticquentCollection;
use \Elasticquent\ElasticquentResultCollection as ResultCollection;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

         //disable foreign key check for this connection before running seeders
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        // Truncate all tables, except migrations
        $tables = Schema::getConnection()->getDoctrineSchemaManager()->listTableNames();
        foreach ($tables as $name) {
            if($name == 'migrations'){
                continue;
            }
            DB::table($name)->truncate();
        }
        
        /**
        // clear out our tracker data
        $tracker_tables = DB::connection('tracker')->select('SHOW TABLES');
        DB::connection('tracker')->statement('SET FOREIGN_KEY_CHECKS=0;');
        foreach ($tracker_tables as $track) {
            DB::connection('tracker')->table($track->Tables_in_tracker)->truncate();
        }
        **/

        $this->call(UsersTableSeeder::class);
        $this->call(TitlesTableSeeder::class);
        //only needed for test data
       // $this->call(ItemsTableSeeder::class);
        
        Title::reindex();
        Author::reindex();
        Isbn::reindex();
        User::reindex();
        Rlist::reindex();

        /** Notification module deactivated
        Notifynder::addCategory("item_sold", "Your copy of {extra.title} ref: {extra.ref} has sold.");

        Notifynder::addCategory("item_bought", "You have bought {extra.title} ref: {extra.ref}. Please contact the seller and make payment. To collect the item you will need proof of purchase.");

        Notifynder::addCategory("admin_item_bought", "Title:{extra.title} ref: {extra.ref} has sold. From seller:{extra.seller} to buyer:{extra.buyer}.");

        Notifynder::addCategory("admin_item_removed", "Title::{extra.title} ref: {extra.ref} has been removed by the seller.");

        Notifynder::addCategory("sale_cancelled", "The sale of Title::{extra.title} ref: {extra.ref} has been cancelled.");
        **/

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
      //  DB::connection('tracker')->statement('SET FOREIGN_KEY_CHECKS=1;');

        Model::reguard();
    }
}
