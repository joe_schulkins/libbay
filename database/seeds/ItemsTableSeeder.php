<?php


use Illuminate\Database\Seeder;
use Livbay\Item\Item as Item;
use Carbon\Carbon;


/**
 * 
 */
class ItemsTableSeeder extends Seeder
{
	public function run(){
		$items = DB::table('items')->where('status','purchased')->orwhere('status', 'collect')->get();

		$faker = Faker\Factory::create();
		
		foreach($items as $item){
			$user = $faker->numberBetween($min = 3, $max = 30);
			$item = Item::find($item->id);
            $item->enquired()->attach([$user => ['relationship'=>'enquired']]);
            $item->collect = Carbon::create('2016', $faker->numberBetween($min = 3, $max = 12), $faker->numberBetween($min = 1, $max = 28),1);
            $item->save();
		}
	}
}