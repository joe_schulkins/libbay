<?php


use Illuminate\Database\Seeder;

/**
 * 
 */
class UsersTableSeeder extends Seeder
{
	
	public function run()
	{
		factory('Livbay\User\User', 1)->create(['lname'=>'Admin','fname'=>'Admin','email'=>'sjinfsup@liv.ac.uk','admin'=>'1','pay'=>'false', 'valid'=>'1']);
		factory('Livbay\User\User', 1)->create(['lname'=>'Schulkins','fname'=>'Joe','email'=>'j_schulkins@hotmail.com','pay'=>'07702621156','admin'=>'1', 'valid'=>'1']);
		//test users
		/**
		factory('Livbay\User\User', 1)->create(['lname'=>'Schulkins','fname'=>'Joe','email'=>'jschulkins@gmail.com','admin'=>'0', 'valid'=>'1']);
		factory('Livbay\User\User', 10)->create();
		**/
		factory('Livbay\User\User', 100)->create(['valid'=>'1']);
		
	}
}