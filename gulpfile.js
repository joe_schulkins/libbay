/*var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */
/*
elixir(function(mix) {
    mix.sass('app.scss');
});
*/
var elixir = require('laravel-elixir');

require('laravel-elixir-imagemin');

var bowerDir = './resources/assets/vendor/';

// file path to optional template directory
var tempDir = './resources/assets/vendor/templates/livuni/';
 
var lessPaths = [
    bowerDir + "bootstrap/less",
    bowerDir + "font-awesome/less",
    bowerDir + "bootstrap-select/less"
];
 
elixir(function(mix) {
    mix.imagemin();
        mix.less('app.less', 'public/css', { paths: lessPaths })
        mix.styles([
            'css/liv.css',
            'css/reset.css'
            ], 'public/css/livuni.css', tempDir)
        .scripts([
            'jquery/dist/jquery.min.js',
            'jquery-ui/jquery-ui.min.js',
            'custom',
            'bootstrap/dist/js/bootstrap.min.js',
            'bootstrap-select/dist/js/bootstrap-select.min.js'
            ], 'public/js/vendor.js', bowerDir)
        .copy(tempDir + 'css/fonts.css', 'public/css/fonts.css')
        .copy(bowerDir + 'bootstrap/fonts', 'public/fonts')
        .copy(bowerDir + 'font-awesome/fonts', 'public/fonts')
        .copy(bowerDir + 'font-awesome/fonts', 'public/build/fonts')
        .copy(bowerDir + 'bootstrap/fonts', 'public/build/fonts')
        .copy(bowerDir + 'jquery-ui/themes/smoothness/', 'public/css/jquery-ui/themes/smoothness/');
 
 	 mix.version([
 	 	'public/css/',
 	 	'public/js/'
 	 	]);

});
