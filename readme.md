## 'Pass the Book'

Book selling/donation platform for the University of Liverpool Library

### License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

### Elasticsearch

Elasticsearch provides indexing support for our models and needs to be installed on the server. These intstructions show how to run it as a [service](https://www.elastic.co/guide/en/elasticsearch/reference/current/setup-service-win.html).

#### 