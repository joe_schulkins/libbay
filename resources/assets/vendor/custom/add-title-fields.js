$(function(){
    			var max_auth      = 100; //maximum input boxes allowed
    			var max_isbn      = 100; //maximum input boxes allowed
    			var auth_wrapper         = $(".author-rows"); //Fields wrapper
    			var isbn_wrapper         = $(".isbn-rows"); //Fields wrapper
    			var add_author     = $(".add_field_author"); //Add button ID
    			var add_isbn    = $(".add_field_isbn"); //Add button ID
    
    			if(jQuery(auth_wrapper).children().length === 0){
                    var x = 0; //initlal author box count
                }
                else{
                    var x = jQuery(auth_wrapper).children().length;
                }
    			var y = 0; //initial isbn count
    			
    			jQuery(add_author).click(function(e){ //on add input button click
        			e.preventDefault();
        			if(x < max_auth){ //max input box allowed
            			$(auth_wrapper).append('<div class="form-group row"><div class="col-xs-12 col-md-1"><label for="lname" class="form-control-label">Last Name:</label></div><div class="col-xs-6 col-md-4"><input name="author['+x+'][lname]" type="text" class="form-control"></div><div class="col-xs-6 col-md-1"><label for="fname" class="form-control-label">First Name:</label></div><div class="col-xs-6 col-md-4"><input class="form-control" name="author['+x+'][fname]" type="text"></div><input type="hidden" name="author['+x+'][newid]" value="n'+x+'"/><div class="col-xs-2"><button type="button" class="btn btn-default btn-sm remove_author"><span class="glyphicon glyphicon-minus" aria-hidden="true" aria-label="remove row"></span></button></div></div>'); //add input box
        			}
        			x++; //text box increment
    			});

    			jQuery(add_isbn).click(function(e){ //on add input button click
        			e.preventDefault();
        			if(y < max_isbn){ //max input box allowed
            			$(isbn_wrapper).append('<div class="form-group row"><div class="col-xs-12 col-md-2"><label for="isbn" class="form-control-label">ISBN:</label></div><div class="col-xs-10 col-md-8"><input class="form-control" name="isbn['+y+'][isbn]" type="text"></div><input type="hidden" name="isbn['+y+'][newid]" value="n'+y+'"/><div class="col-xs-2"><button type="button" class="btn btn-default btn-sm remove_isbn"><span class="glyphicon glyphicon-minus" aria-hidden="true" aria-label="remove row"></span></button></div></div>'); //add input box
        			}
        			y++; //text box increment
    			});
    
    			jQuery(auth_wrapper).on("click",".remove_author", function(e){ //user click on remove author
        			e.preventDefault();

        			$(this).parent('div').remove(); x--;
    			});

    			jQuery(isbn_wrapper).on("click",".remove_isbn", function(e){ //user click on remove isbn
        			e.preventDefault(); 
        			$(this).parent('div').remove(); y--;
    			});
            });