 
$.datepicker.setDefaults({
  showOn: "both",
  buttonImageOnly: true,
  buttonImage: "/images/calendar.gif",
  buttonText: "Calendar",
  dateFormat: "dd-mm-yy"
});

/**
 * set the default date so it starts three days from today
 * and the minimum date is 3 days hence. No weekekends can be chosen.
 */
  $(function() {
    $( ".datepicker" ).datepicker({
    	beforeShowDay: $.datepicker.noWeekends, 
    	defaultDate: +3,
    	minDate: 3
    });
  });

  /**
   * Datepicker without date restrictions eg for entering publication dates
   */
  $(function() {
    $( ".pubdatepicker" ).datepicker({
      changeYear: true,
      changeMonth: true,
      yearRange: "c-80:c+5"

    });
  });
