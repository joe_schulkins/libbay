@extends('app')

@section('content')


<div class="row">
	<div class="col-3">
		<div class="panel panel-default">
			<div class="panel-heading">Items for Collection</div>
  			<div class="panel-body">{!! $col_item->render() !!}</div>
		</div>
	</div>
</div>

@stop