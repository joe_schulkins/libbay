@extends('app')

@section('content')

<h1>Editing {{ $edit->type }}</h1>
<p class="lead">Edit and save the record below</p>
<hr>

@if(Session::has('flash_message'))
    <div class="alert alert-success">
        {{ Session::get('flash_message') }}
    </div>
@endif

@if($edit->type == 'user')
	{!! Form::model($edit, [
    	'method' => 'PATCH',
    	'route' => ['userUpdate', $edit->id]
	]) !!}
@elseif($edit->type == 'item')

<div class="row">
<div class="col-xs-12">
<h3> {{ ucwords($edit->title->first()->title) }}</h3>
</div>
</div>
	{!! Form::model($edit, [
    	'method' => 'PATCH',
    	'route' => ['itemUpdate', $edit->id]
	]) !!}
@else
{{-- dd($edit) --}}
	{!! Form::model($edit, [
    	'method' => 'PATCH',
    	'route' => ['updateBib', $edit->id]
	]) !!}
@endif



	@if($edit->type == 'user')
		@include('user.form')
	@elseif($edit->type == 'item')
		@include('user.item-form')
	@else
		@include('title.form')
	@endif

<div class="row">
<div class="col-xs-12">
	{!! Form::submit('Update Record', ['class' => 'btn btn-primary']) !!}
	<a href="{{ route('cancel') }}">Cancel</a>
	{!! Form::close() !!}
	</div>
</div>

	@if($edit->items)
<div class="row">
<legend>Items:</legend>
<table class="table">
<thead>
<th>Reference Number</th>
<th>Seller</th>
<th>Buyer</th>
</thead>
<tbody>

@foreach($edit->items as $item)
<tr>
<td><a href="{{ route('itemEdit', $item->id) }}">{{ $item->ref }}</a></td>
<td><a href="{{ route('userEdit', $item->listed->first()->id) }}">{{ $item->listed->first()->fname }} {{ $item->listed->first()->lname }}</a></td>
@if($item->enquired->count() > 0)
<td><a href="{{ route('userEdit', $item->enquired->first()->id) }}">{{ $item->enquired->first()->fname }} {{ $item->enquired->first()->lname }}</a>></td>
@endif
</tr>
@endforeach

</tbody>
</table>
</div>

@endif



@if($edit->type == 'user')
<hr>
 <div class="row">
	<div class="col-xs-12"><h3>User as seller:</h3></div>
</div>

 		<div class="row">
	<div class="col-xs-12">{!! $edit->listed->render() !!}
 	</div>
 </div>
  <div class="row">
	 	<div class="col-xs-12"><h3>User as buyer:</h3></div>
	 	</div>
 		<div class="row">
	<div class="col-xs-12">
	{!! $edit->enquired->render() !!}
 	</div>
 </div>
@endif



</div>
</div>

@stop