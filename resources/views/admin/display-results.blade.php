@extends('app')

@section('content')

<table class="table table-striped">
{!! $results->render() !!}
<th>Resource Value</th>
<th>Resource Name</th>

@foreach($results as $result )
		<tr>
			<td>
			@if($result['res']['type'] == 'user')
				<a href="{{ route($result['res']['type'].'Edit', $result['res']['id']) }}">
			@endif
			<a href="{{ route($result['res']['type'].'Edit', $result['res']['id']) }}">{{ $result['value'] }}</a></td>
			<td>{{ $result['type'] }}</td>
		<tr>	
@endforeach

</table>
{!! $results->render() !!}

@stop