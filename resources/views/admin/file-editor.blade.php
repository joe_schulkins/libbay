@extends('app')

@section('content')

@include('tinymce::tpl')


<div class="row">
	<div class="col-xs-12">
	<h2>Edit text system text files</h2>
	</div>
</div>
<hr>

@if(!$contents)
<div class="row">
	<div class="col-xs-8">

		{!! Form::open(['action'=>'PagesController@editThisText']) !!}
		{!! Form::select('status',['Text'=>['text-terms-conditions'=>'Terms and Conditions', 'text-collect' => 'Collect text','text-about'=>'About Page','text-contact'=>'Contact Page'],'Emails'=>['emails-item-bought-buyer'=>'Item Bought Buyer', 'emails-item-bought-seller'=>'Item Bought Seller','emails-password-reset'=>'Password Reset', 'emails-sale-cancelled'=>'Sale Cancelled', 'emails-user-approved'=>'User Approved', 'emails-user-registered'=>'User Registered' ]],null, ['class'=> 'form-control']) !!}
	</div>
	<div class="col-xs-4">
		{!! Form::submit('Edit Text', ['class'=> 'btn btn-primary form']) !!}
		<button class="btn btn-default"><a href="{{ route('cancel') }}">Cancel</a></button>
		{!! Form::close() !!}
	</div>
</div>
@else
@if($contents == 'success')
{{ dd($contents) }}
@else
<div class="row">
	<div class="col-xs-8">
		{!! Form::open(['action'=>'PagesController@saveText']) !!}
		{!! Form::textarea('tinymce', $contents, ['id'=>'tinymce'] ) !!}
		{!! Form::hidden('path', $path) !!}
	</div>
	<div class="col-xs-4">
		{{-- Form::submit('Save Text', ['class'=> 'btn btn-primary form']) --}}
		<button class="btn btn-default"><a href="{{ route('cancel') }}">Cancel</a></button>
		{!! Form::close() !!}
	</div>
</div>
@endif
@endif

@stop