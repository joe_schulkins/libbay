@extends('app')

@section('content')


<div class="page-title">
	<h3>Admin dashboard</h3>
</div>
<div class="row">
	<div class="col-xs-12 col-md-12">
		<div class="panel panel-success">
			<div class="panel-heading"><a href="{{ route('adminSearch') }}" id="notifications">Search</a></div>
			<div class="panel-body">
				@include('admin.search')
			</div>
		</div>	
	</div>
</div>
<div class="row">
	<div class="col-xs-12 col-md-4">
		<div class="panel panel-success">
			<div class="panel-heading"><a href="{{ route('adminNewUsers') }}">New Users</a></div>
			<div class="panel-body">
				<div class="row">
					<div class="col-xs-12">
						<a href="{{ route('adminNewUsers') }}"><i class="fa fa-user-plus"></i> {{ $admin['u_count'] }}</a>
					</div>
				</div>
			</div>
		</div>	
	</div>
	<div class="col-xs-12 col-md-4">
		<div class="panel panel-success">
			<div class="panel-heading"><a href="{{ route('adminNewItems') }}">New Items</a></div>
			<div class="panel-body">
				<div class="row">
					<div class="col-xs-12">
						<a href="{{ route('adminNewItems') }}"><i class="fa fa-book"></i> {{ $admin['i_count'] }}</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	{{-- <div class="col-xs-12 col-md-4">
	<div class="panel panel-success">
		<div class="panel-heading"><a href="{{ route('allNotifications', $user->getRouteKey()) }}" id="notifications">New Notifications</a></div>
		<div class="panel-body">
			@if($user->countNotificationsNotRead() > 0)
			<a href="{{ route('allNotifications', $user->getRouteKey()) }}" id="notifications">{{$user->countNotificationsNotRead()}}</a>
			@else
			No new notifications.
			@endif
		</div>
	</div>
</div>	--}}	
</div>

<div class="row">
	<div class="col-xs-12 col-md-4">
		<div class="panel panel-success">
			<div class="panel-heading"><a href="{{ route('collectItem') }}" id="collection">Items for Collection</a></div>
			<div class="panel-body">
				<a href="{{ route('collectItem') }}"><i class="fa fa-book"></i> {{ $admin['collect'] }}</a>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-md-4">
		<div class="panel panel-success">
			<div class="panel-heading"><a href="{{ route('displayClearShelf') }}" id="clearShelf">Items for Removal</a></div>
			<div class="panel-body">
				<a href="{{ route('displayClearShelf') }}"><i class="fa fa-book"></i> {{ $admin['clr'] }}</a>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-md-4">
		<div class="panel panel-success">
			<div class="panel-heading">Functions</div>				
			<div class="panel-body">
				<div class="btn-group" role="group">
					<a href="/title/create"><button type="button" class="btn btn-xs btn-primary">Add Item</button></a>
					<a href="/admin/file-editor"><button type="button" class="btn btn-xs btn-primary">File Editor</button></a>
				</div>
			</div>	
		</div>
	</div>			
</div>



<div class="row">
	<div class="col-xs-2 col-md-4">
		<div class="panel panel-info">
			<div class="panel-heading"><a href="{{ route('listed', $user->getRouteKey()) }}">Books you are selling</a></div>
			<div class="panel-body">
				@if(count($user->listed) > 0)
				<a href="{{ route('listed', $user) }}">{{ count($user->listed)  }}</a>
				@else
				{{ count($user->listed)  }}					
				@endif
			</div>
		</div>
	</div>
	<div class="col-xs-2 col-md-4">
		<div class="panel panel-info">
			<div class="panel-heading"><a href="{{ route('enquired',  $user->getRouteKey()) }}">Books you are buying</a></div>
			<div class="panel-body">
				@if(count($user->enquired) > 0)
				<a href="{{ route('enquired',  $user->getRouteKey()) }}">{{ count($user->enquired)  }}</a>
				@else
				{{ count($user->enquired)  }}
				@endif
			</div>
		</div>
	</div>
</div>
</div>

@stop