<table class="{{ $class or 'table' }}">
    @if(count($columns))
	<thead>
		<tr>
        @foreach($columns as $c)
            <th {!! $c->getClasses() ? ' class="' . $c->getClassString() . '"' : '' !!}>
                @if($c->isSortable())
                    <a href="{{ $c->getSortURL() }}">
                        {!! $c->getLabel() !!}
                        @if($c->isSorted())
                            @if($c->getDirection() == 'asc')
                                <span class="fa fa-sort-asc"></span>
                            @elseif($c->getDirection() == 'desc')
                                <span class="fa fa-sort-desc"></span>
                            @endif
                        @endif
                    </a>
                @else
                    {{ $c->getLabel() }}
                @endif
            </th>
        @endforeach
        
        <!-- add user action column -->
        <th>User Action</th>
		</tr>
	</thead>
    @endif
	<tbody>
        @if(count($rows))
            @foreach($rows as $r)

        <tr>
            @foreach($columns as $c)
                <td {!! $c->getClasses() ? ' class="' . $c->getClassString() . '"' : '' !!}>
                 @if($c->hasRenderer())
                    {{-- Custom renderer applied to this column, call it now --}}
                    {!! $c->render($r) !!}
                    @else
                    {{-- Use the "rendered_foo" field, if available, else use the plain "foo" field --}}
                        {!! $r->{'rendered_' . $c->getField()} or $r->{$c->getField()} !!}
                    @endif
                </td>
            @endforeach
    
        <!-- add user action -->
        <td>
        <div class="btn-group closed">
                    <a class="btn btn-primary" href="#"><i class="fa fa-user fa-fw"></i> User</a>
                    <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#">
                    <span class="fa fa-caret-down"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ route('adminApproveUser', $r->getRouteKey()) }}"><i class="fa fa-check fa-fw"></i> Approve</a></li>
                        <li><a href="{{ route('adminEditUser', [$r->getRouteKey(), $r->type]) }}"><i class="fa fa-pencil fa-fw"></i> Edit</a></li>
                        <li><a href="{{ route('adminUserDelete', [$r->getRouteKey()]) }}"><i class="fa fa-trash-o fa-fw"></i> Delete</a></li>
                        <li><a href="{{ route('adminBanUser', $r->getRouteKey()) }}"><i class="fa fa-ban fa-fw"></i> Ban</a></li>
                        <li class="divider"></li>
                        <li><a href="{{ route('adminMakeAdminUser', $r->getRouteKey()) }}"><i class="i"></i> Make admin</a></li>
                    </ul>
                </div>
               </td>
        </tr>

            @endforeach
        @endif
	</tbody>
</table>

@if(is_object($rows) && class_basename(get_class($rows)) == 'LengthAwarePaginator')
    {{-- Collection is paginated, so render that --}}
    {!! $rows->render() !!}
@endif
