@extends('app')

@section('content')

<div class="row">
	<div class="col-3">
		<div class="panel panel-default">
			<div class="panel-heading">New Users</div>
  			<div class="panel-body">{!! $new_user->render() !!}</div>
		</div>
	</div>
</div>

@stop