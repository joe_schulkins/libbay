	{!! Form::open(['route'=>'adminSearch', 'method'=>'GET', 'role'=> 'form', 'class'=>'form-inline']) !!}
		<div class="form-group" id="search-bar">
			{!! Form::input('search','query', Input::get('query', ''), ['Placeholder'=>'Search', 'class'=> 'form-control']) !!}
		</div>
		<div class="form-group">
			{!! Form::select('type',['all'=>'All', 'authors'=>'Authors','isbns'=>'ISBN', 'items'=>'Items', 'titles'=>'Titles', 'users' => 'Users'],null, ['class'=> 'form-control']) !!}
		</div>
		<div class="form-group">
			<tr>
				{!! Form::submit('Search', ['class'=> 'btn btn-primary']) !!}
			</tr>
		</div>	
		</table>
	{!! Form::close() !!}
