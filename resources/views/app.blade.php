<!DOCTYPE html>
<html class="no-skrollr">
<head>
	<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="description" content="A global strategy for advancement of learning and ennoblement of life">
	<title>{{ Config::get('bosbooks.name') }}</title>
    <link rel="icon" href="{{ asset('images/favicon.ico') }}" type="image/x-icon" />
	
  <link rel="stylesheet" href="{{ elixir('css/app.css') }}">
  <link rel="stylesheet" href="{{ elixir('css/livuni.css') }}">
  <link rel="stylesheet" href="{{ elixir('css/fonts.css') }}">
  <link rel="stylesheet" href="/css/jquery-ui/themes/smoothness/jquery-ui.min.css">

  <link href='https://fonts.googleapis.com/css?family=PT+Serif' rel='stylesheet' type='text/css'>

  <script type="text/javascript"  src="{{  elixir('js/vendor.js') }}"></script>

  {{-- Adobe typekit fonts --}}
  <script src="https://use.typekit.net/aiw8yji.js"></script>
  <script>try{Typekit.load({ async: true });}catch(e){}</script>

</head>

<body>

@if(Config::get('app.template') != false)
  @include(Config::get('app.template').'.google-tag')
@endif


		<div class="header" id="nav-header">
    
    
		<div id="top-tier">
    {{-- remove top-tier
  <div class="container">
    <div class="inner">
      <nav id="country-pages">
        <ul>
          <li class="home-link"><a href="//www.liverpool.ac.uk/">University home</a></li>
          <li>Welcome:</li>
          <li><a href="//www.liverpool.ac.uk/cn/">中文</a></li>
          <li><a href="//www.liverpool.ac.uk/ara/">عربي</a></li>
          <li><a href="//www.liverpool.ac.uk/esp/">Español</a></li>
        </ul>
      </nav>

     <nav id="global-nav">
        <ul>
          <li class="a-z"><a href="//www.liverpool.ac.uk/a-z/">Site A-to-Z</a></li>
          <li>Login:</li>
          <li class="staff-login"><a href="http://staff.liverpool.ac.uk/">Staff</a></li>
          <li class="student-login"><a href="http://student.liverpool.ac.uk/">Students</a></li>
        </ul>
      </nav> 
      <div style="clear:both"></div>
    </div>
  </div> --}}
</div> 


<header id="masthead" role="banner">
  <div class="row">
    <div class="col-xs-6">
      {{-- Uni Header --}}
        <a href="#main-content" title="Skip Navigation" id="skip-nav" class="visuallyhidden">Skip navigation</a>
      <div id="logo">
        <h2><a id="uni-home-link" href="//www.liverpool.ac.uk/" title="University of Liverpool Home">University of Liverpool - </a></h2>
      </div>
      <div id="dept-link-holder">
        <div id="dept-link">
          <h2>library</h2>
        </div>
      </div>
      {{-- end Uni Header --}}
      </div>
  </div>

{{-- Pass the Book navigation --}}
  <div class="row">
    <div class="col-xs-12">
      <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="ptb-nav" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
            </div>
            <div id="ptb-nav" class="navbar-collapse collapse">
              <ul class="nav navbar-nav navbar-left">
                <li><a href="/">Home</a></li>
                <li><a href="/about">About</a></li>
                <li><a href="/contact">Contact Us</a></li>    
              </ul>
              <ul class="nav navbar-nav navbar-right">
                @include('partials.navbar-main')
              </ul>
            </div>
          </div>
        </nav>
      </div>
    </div>
    {{-- end of Pass the Book navigation --}}
      
      {{-- remove university search box 
      <div id="global-and-search" class="different">
                       
                       <hr/>
                       <form id="search" method="get" action="//www.liverpool.ac.uk/search/results/index.php" name="gs" role="search">
      <label class="visuallyhidden" for="query">Search the University</label>
      <input type="text" name="query" size="25" id="query" maxlength="255" value="" class="search-input" placeholder="search"/>
                          <input class="input" type="image" src="https://www.liverpool.ac.uk/files/2012-working-demo/search-button.gif" value="Search" />
     <input type="hidden" name="start_rank" value="1" />
                                        <input type="hidden" name="page" value="1" />
                          <input type="hidden" name="collection" value="main-collection" />
    </form>
               </div>
    --}}

    </header>
    {{-- added navbar navbar-default to the class statement --}}
		<nav id="mobile-sticky-header" class="color-base-plus-black-50 clearfix navbar navbar-default">
    <nav class="">
    <div class="header-clear">
    <div id="sticky-logo"><a id="uni-home-link" href="//www.liverpool.ac.uk/" title="University of Liverpool Home">University of Liverpool</a></div>
    <div class="sticky-dept-link"><a href="/">Pass The Book</a></div>

    {{-- Pass the Book navigation --}}
      
        <div class="container-fluid">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#ptb-nav" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
            </div>
            <div id="ptb-nav" class="navbar-collapse collapse">
              <ul class="nav navbar-nav navbar-left">
                <li><a href="/about">About</a></li>
                <li><a href="/contact">Contact Us</a></li>    
              </ul>
              <ul class="nav navbar-nav navbar-right">
                @include('partials.navbar-main')
              </ul>
            </div>
          </div>
        </nav>
    {{-- end of Pass the Book navigation --}}
    
  </div>
  </nav>

  

 

{{-- Pass the Book content --}}
<div class="container">
  <div class="well">
  @if (Session::has('flash_notification.message'))
      <div class="alert alert-{{ Session::get('flash_notification.level') }}">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

          {!! Session::get('flash_notification.message') !!}
      </div>
  @endif

  
  @yield('content')

  @yield('errors')
  </div>
</div>
{{-- end of Pass the Book content --}}

		<div class="footer">
		<div class="container">
    <div class="inner">
      <div id="address">
        <p class="vcard"> <span class="fn org">University of Liverpool</span><br>
          <span class="adr"> <span class="locality">Liverpool</span> <span class="postal-code">L69 3BX</span>, <span class="country-name">United Kingdom</span><br>
          </span> <span class="tel">+44 (0)151 794 2000</span> </p>
          
      </div>
    </div>
  </div>
  <div class="container ">
    <div class="inner copy-legal">
      <nav id="copyright">
        <ul>
          <li>© University of Liverpool - a member of <a href="https://www.russellgroup.ac.uk/" target="_blank">The Russell Group</a></li>
        </ul>
      </nav>
      <nav id="legal">
        <ul>
          <li><a href="https://www.liverpool.ac.uk/maps/">Map</a> </li>
          <li><a href="https://www.liverpool.ac.uk/legal/">Terms and Conditions</a></li>
          <li><a href="https://www.liverpool.ac.uk/accessibility/">Accessibility</a> </li>
        </ul>
      </nav>
      <div style="clear:both"></div>
    </div></div>
<!-- END ULIV HEADER -->
        </div>

	{{-- Pass the Book scripts --}}  
  <script>
    jQuery('#flash-overlay-modal').modal();
  </script>
  {{-- end of Pass the Book scripts --}}  
	
</body>

</html>
