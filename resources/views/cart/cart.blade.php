<!-- no need for formatting as this content is dropped in to our master page -->
@extends('app')

<!-- find the section in app.blade.php that we referenced - in this case 'content' -->
@section('content')
  
@if($notAvailable != null)
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-danger">
                The following items have just been bought and are no longer available for sale:
                @foreach($notAvailable as $row)
                    <br /><br />{{ $row->name }} £{{ $row->price }}
                @endforeach
            
        </div>
    </div>
</div>
@endif

@if(sizeof($cart) > 0)

<table class="table table-ellip table-striped ">
    <thead>
        <tr>
            <th>Product</th>
            <th>Qty</th>
            <th>Item Price</th>
            <th>Subtotal</th>
            <th></th>
        </tr>
    </thead>

    <tbody>

    @foreach($cart as $row)

        <tr>
            <td>
                <p><strong>{{ ucwords($row->name) }}</strong></p>
            </td>
            <td>{{ $row->qty }}</td>
            <td>£{{ $row->price }}</td>
            <td>£{{ $row->subtotal }}</td>
            <td>
                {!! Form::open([ 'method'  => 'get', 'route' => [ 'removeFromCart', $row->rowid ] ]) !!}
                    {!! Form::hidden('id', $row->rowid) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                {!! Form::close() !!}
            </td>
       </tr>

    @endforeach
    <tr>
        <td>Total:</td><td>£{{ $total }}</td>
    </tr>
    <tr>
        <td><a href="{{ url('/item/checkoutCart') }}"><button type="submit" class="btn btn-success">Checkout</button></a></td>
        <td><a href="{{ url('home') }}"><button type="submit" class="btn btn-danger">Cancel</button></a></td>    
    </tr>

    </tbody>
</table>
@else
 Your shopping basket is empty.
 @endif


@endsection