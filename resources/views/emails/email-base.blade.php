<h1>{{ Config::get('bosbooks.name') }}</h1>

@yield('content')

<hr>
<h4>Terms and Conditions</h4>
@include('partials.text-terms-conditions')