<!-- no need for formatting as this content is dropped in to our master page -->
@extends('emails.email-base')

<!-- find the section in app.blade.php that we referenced - in this case 'content' -->
@section('content')
<p>
Item Details
</p>
@if($data['recip'] == 'buyer')
	@include('partials.emails-item-bought-buyer')
@else
	@include('partials.emails-item-bought-seller')

@endif
<hr>
<table class="tables">
<tr>
  <td>Title: </td><td>{{ ucwords($item->title->first()->title) }}</td>
</tr>
<tr>
  <td>Price: </td><td>£{{ $item->price }}</td>
</tr>

<tr>
  <td>Payment details: </td> 
  @if($item->price > 0)
  	<td>{{ $data['pay'] }}</td>
  @else
  	<td>No payment to make</td>
  @endif
</tr>

<tr>
  <td>Condition: </td><td>{{ $item->condition }}</td>
</tr>
<tr>
  <td>Comments: </td><td>{{ $item->comments }}</td>
</tr>
<tr>
  <td>Reference: </td><td>{{ $item->ref }}</td>
</tr>
<tr>
  <td>Date Sold: </td><td>{{ $item->updated_at }}</td>
</tr>
</table>
</hr>
@stop
