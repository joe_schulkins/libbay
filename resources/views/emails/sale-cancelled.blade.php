<!-- no need for formatting as this content is dropped in to our master page -->
@extends('emails.email-base')

<!-- find the section in app.blade.php that we referenced - in this case 'content' -->
@section('content')

@include('partials.emails-sale-cancelled')
<hr>
<table class="tables">
<tr>
  <td>Title: </td><td>{{ ucwords($item->title->first()->title) }}</td>
</tr>
<tr>
  <td>Price: </td><td>£{{ $item->price }}</td>
</tr>
<tr>
  <td>Condition: </td><td>{{ $item->condition }}</td>
</tr>
<tr>
  <td>Comments: </td><td>{{ $item->comments }}</td>
</tr>
<tr>
  <td>Reference: </td><td>{{ $item->ref }}</td>
</tr>
<tr>
  <td>Date Cancelled: </td><td>{{ \Carbon\Carbon::now() }}</td>
</tr>
</table>
</hr>

@stop