<!-- no need for formatting as this content is dropped in to our master page -->
@extends('emails.email-base')

<!-- find the section in app.blade.php that we referenced - in this case 'content' -->
@section('content')

@include('partials.emails-user-approved')

@stop