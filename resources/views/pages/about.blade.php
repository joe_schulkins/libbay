<!-- no need for formatting as this content is dropped in to our master page -->
@extends('app')

<!-- find the section in app.blade.php that we referenced - in this case 'content' -->
@section('content')

<h2>About This Service</h2>
<hr>
@include('partials.text-about')

<hr>
@include('partials.text-terms-conditions')


<!-- end our insert -->
@stop