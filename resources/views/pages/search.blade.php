@extends('app')

@section('content')	



	<div class="form-group" id="search-bar">
	{!! Form::open(['url'=>'listed/search', 'method'=>'GET']) !!}
		<div class="row">
			<div class="col-xs-12 col-md-8">
				{!! Form::input('search','query', Input::get('query', ''), ['Placeholder'=>'Search for Title, Author, ISBN ...', 'class'=> 'search-query form-control']) !!}
			</div>
			<div class="col-xs-12 col-md-4">
				{!! Form::submit('Find a Book', ['class'=> 'btn btn-primary']) !!}
				Or <a href="/title">browse</a> all titles.
			</div>

		</div>	
	{!! Form::close() !!}
	</div>

<div id="search-results"></div>

<table class="table table-striped">
{!! $rlists->render() !!}
<th>Reading List Name</th>
{{-- <th>No. of Titles</th> --}}
{{-- dd($rlists) --}}
@foreach($rlists as $list )
		<tr>
			<td><a href="{{ url('/title/readinglist', $list['id']) }}">{{ $list['rlist_name'] }}</a></td>
			{{-- <td>{{ $list['bk_count'] }}</td> --}}
		<tr>	
@endforeach

</table>
{!! $rlists->render() !!}

@stop