<p>
Your item has sold, the buyer now has 7 days to pay for the item and arrange collection. If 7 days pass and the buyer has not completed these steps, then this item will be relisted by library staff.
</p>