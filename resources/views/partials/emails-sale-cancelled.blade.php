<p>
Item Details
</p>
<p>
The sale of your item has been cancelled by the buyer. We will automatically relist it for you unless you instruct us otherwise.
</p>