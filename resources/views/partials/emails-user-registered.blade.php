<p>{{$user->fname}}, you are now registered. Once you have been approved by an administrator you will be free to buy and sell books.</p>
<p>Enjoy!</p>