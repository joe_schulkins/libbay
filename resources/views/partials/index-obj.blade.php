<tr>
			@if($title->subtitle)
				<td style="width:40%">
					<h2>
						<a href="{{ url('/title', $title->id) }}">{{ ucwords($title->title) }}</a>:
					</h2>
					<h3>
						{{ $title->subtitle }}
					</h3>
				</td>
			@else
				<td>
					<h2>
						<a href="{{ url('/title', $title->id) }}">{{ ucwords($title->title) }}</a>
					</h2>
				</td>
			@endif
			<td style="width:15%">
			@if($title->edition)
				{{ $title->edition }}
			@endif
			</td>
			<td style="width:10%">
			@if($title->volume)
				{{ $title->volume }}
			@endif
			</td>
			<td style="width:15%">
			@if($title->pubdate)
				{{ $title->pubdate->year }}
			@endif
			</td>
			@if($isbn != false)
			<td style="width:20%">
				<img src="http://www.syndetics.com/index.aspx?isbn={{ $isbn['isbn'] }}/SC.GIF&amp;client=livea" alt="{{ $title->title }} book jacket" height="80px">				
			</td>
			@endif
	</tr>