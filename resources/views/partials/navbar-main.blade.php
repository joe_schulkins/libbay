			@if (\Auth::check())
				<?php $user = \Auth::user();?>
				<li ><a href="/home" id="logout">Dashboard</a></li>
				<li><a href="{{ route('viewCart') }}" id="shopping_cart"><i class="fa fa-shopping-cart"></i>
				<?php $i=0; ?>
				@foreach(Cart::content() as $row)
					<?php $i+= $row->qty; ?>
				@endforeach
				 {{ $i }}</a></li>

				{{--   @if ($user->countNotificationsNotRead() > 0)
					<li ><a href="{{ route('allNotifications', $user->getRouteKey()) }}" id="notifications"><i class="fa fa-envelope"></i>  {{$user->countNotificationsNotRead()}} </a></li>
				@endif
				--}}
				 <li ><a href="/auth/logout" id="logout">Logout</a></li>

			@else
				<li ><a href="/auth/login" id="login">Login</a></li>
				<li ><a href="/auth/register" id="register">Register</a></li>

			@endif