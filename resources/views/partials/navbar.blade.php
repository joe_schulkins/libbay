			{!! Form::open(['url'=>'listed/search']) !!}
			<div class="form-group" id="search-bar">
			
				{!! Form::input('search','q', null, ['Placeholder'=>'Search for Title, Author, ISBN ...', 'class'=> 'search-query form-control']) !!}

			</div>
			<div class="form-group">
				{!! Form::submit('Find a Book', ['class'=> 'btn btn-primary']) !!}

			</div>	

			{!! Form::close() !!}

			@if (\Auth::check())
				<li ><a href="/home/edit" id="profile">{{ \Auth::user()->pluck('fname') }} {{ \Auth::user()->pluck('lname') }}</a></li>
				<li ><a href="/home" id="logout">Dashboard</a></li>
            	<li ><a href="/auth/logout" id="logout">Logout</a></li>

			@else
				<li ><a href="/auth/login" id="login">Login</a></li>
				<li ><a href="/auth/register" id="register">Register</a></li>

			@endif