@extends('app') 
@section('content')


<div class="row">
	<div class="col-xs-6 text-center">
		<h3><strong>Item Cover Sheet</strong></h3>
	</div>
</div>
<div class="bib_detail">
	<table class="table borderless">
		<tr>
			<td>
			<h4>{{ ucwords($item['title'][0]['title']) }} 
				@if($item['title'][0]['subtitle'] !== null)
				<div class="small">
					{{ ucwords($item['title'][0]['subtitle']) }}
				</div>
				@endif
			</h4>
			</td>
		</tr>
		@if($item['title'][0]['edition'] != null)
		<tr>
			<td>Edition: {{ $item['title'][0]['edition'] }}</td>
		</tr>
		@endif
		<tr>
			<td>Price: &pound;{{ $item['price'] }}</td>
		</tr>
		<tr>
			<td><img src="data:;base64,{{ $code }}" alt="" /></td>
		</tr>
		<tr>

		</tr>
		<tr>

		</tr>

	</table>
	<strong>Terms and Conditions</strong>
	@include('partials.text-terms-conditions')
</div>
@stop