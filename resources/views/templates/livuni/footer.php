<div class="container">
    <div class="inner">
      <div id="address">
        <p class="vcard"> <span class="fn org">University of Liverpool</span><br>
          <span class="adr"> <span class="locality">Liverpool</span> <span class="postal-code">L69 3BX</span>, <span class="country-name">United Kingdom</span><br>
          </span> <span class="tel">+44 (0)151 794 2000</span> </p>
          
      </div>
    </div>
  </div>
  <div class="container ">
    <div class="inner copy-legal">
      <nav id="copyright">
        <ul>
          <li>© University of Liverpool - a member of <a href="https://www.russellgroup.ac.uk/" target="_blank">The Russell Group</a></li>
        </ul>
      </nav>
      <nav id="legal">
        <ul>
          <li><a href="https://www.liverpool.ac.uk/maps/">Map</a> </li>
          <li><a href="https://www.liverpool.ac.uk/legal/">Terms and Conditions</a></li>
          <li><a href="https://www.liverpool.ac.uk/accessibility/">Accessibility</a> </li>
        </ul>
      </nav>
      <div style="clear:both"></div>
    </div></div>
<!-- END ULIV HEADER -->