<div id="top-tier">
  <div class="container">
    <div class="inner">
      <nav id="country-pages">
        <ul>
          <li class="home-link"><a href="//www.liverpool.ac.uk/">University home</a></li>
          <li>Welcome:</li>
          <li><a href="//www.liverpool.ac.uk/cn/">中文</a></li>
          <li><a href="//www.liverpool.ac.uk/ara/">عربي</a></li>
          <li><a href="//www.liverpool.ac.uk/esp/">Español</a></li>
        </ul>
      </nav>
      <nav id="global-nav">
        <ul>
          <li class="a-z"><a href="//www.liverpool.ac.uk/a-z/">Site A-to-Z</a></li>
          <li>Login:</li>
          <li class="staff-login"><a href="http://staff.liverpool.ac.uk/">Staff</a></li>
          <li class="student-login"><a href="http://student.liverpool.ac.uk/">Students</a></li>
        </ul>
      </nav>
      <div style="clear:both"></div>
    </div>
  </div>
</div>
<header id="masthead" role="banner">
<a href="#main-content" title="Skip Navigation" id="skip-nav" class="visuallyhidden">Skip navigation</a>
      <div id="logo">
        <h2><a id="uni-home-link" href="//www.liverpool.ac.uk/" title="University of Liverpool Home">University of Liverpool - </a></h2>
      </div>
      <div id="dept-link-holder">
        <div id="dept-link">
          <h2><a href="//www.liverpool.ac.uk/">Strategy 2026</a></h2>
        </div>
      </div>
      <div id="global-and-search" class="different">
                       
                       <hr/>
                       <form id="search" method="get" action="//www.liverpool.ac.uk/search/results/index.php" name="gs" role="search">
      <label class="visuallyhidden" for="query">Search the University</label>
      <input type="text" name="query" size="25" id="query" maxlength="255" value="" class="search-input" placeholder="search"/>
                          <input class="input" type="image" src="//www.liverpool.ac.uk/files/2012-working-demo/search-button.gif" value="Search" />
     <input type="hidden" name="start_rank" value="1" />
                                        <input type="hidden" name="page" value="1" />
                          <input type="hidden" name="collection" value="main-collection" />
    </form>
               </div>

    </header>