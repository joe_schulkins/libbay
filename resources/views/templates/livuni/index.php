<!DOCTYPE html>
<html class="no-skrollr">
<head>
	<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="description" content="A global strategy for advancement of learning and ennoblement of life">
	<title>{{ Config::get('bosbooks.name') }}</title>
    <link rel="icon" href="./assets/img/favicon.ico" type="image/x-icon" />
	
  <link rel="stylesheet" href="{{ elixir('css/app.css') }}">
  <link rel="stylesheet" href="/css/jquery-ui/themes/smoothness/jquery-ui.min.css">
  <script type="text/javascript"  src="{{  elixir('js/vendor.js') }}"></script>

</head>

<body>

<!-- Google Tag Manager - new nav object code -->

<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-P4FCNF"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-P4FCNF');</script>


		<div class="header" id="nav-header">
		<div id="top-tier">
  <div class="container">
    <div class="inner">
      <nav id="country-pages">
        <ul>
          <li class="home-link"><a href="//www.liverpool.ac.uk/">University home</a></li>
          <li>Welcome:</li>
          <li><a href="//www.liverpool.ac.uk/cn/">中文</a></li>
          <li><a href="//www.liverpool.ac.uk/ara/">عربي</a></li>
          <li><a href="//www.liverpool.ac.uk/esp/">Español</a></li>
        </ul>
      </nav>
      <nav id="global-nav">
        <ul>
          <li class="a-z"><a href="//www.liverpool.ac.uk/a-z/">Site A-to-Z</a></li>
          <li>Login:</li>
          <li class="staff-login"><a href="http://staff.liverpool.ac.uk/">Staff</a></li>
          <li class="student-login"><a href="http://student.liverpool.ac.uk/">Students</a></li>
        </ul>
      </nav>
      <div style="clear:both"></div>
    </div>
  </div>
</div>
<header id="masthead" role="banner">
<a href="#main-content" title="Skip Navigation" id="skip-nav" class="visuallyhidden">Skip navigation</a>
      <div id="logo">
        <h2><a id="uni-home-link" href="//www.liverpool.ac.uk/" title="University of Liverpool Home">University of Liverpool - </a></h2>
      </div>
      <div id="dept-link-holder">
        <div id="dept-link">
          <h2><a href="//www.liverpool.ac.uk/">Strategy 2026</a></h2>
        </div>
      </div>
      <div id="global-and-search" class="different">
                       
                       <hr/>
                       <form id="search" method="get" action="//www.liverpool.ac.uk/search/results/index.php" name="gs" role="search">
      <label class="visuallyhidden" for="query">Search the University</label>
      <input type="text" name="query" size="25" id="query" maxlength="255" value="" class="search-input" placeholder="search"/>
                          <input class="input" type="image" src="https://www.liverpool.ac.uk/files/2012-working-demo/search-button.gif" value="Search" />
     <input type="hidden" name="start_rank" value="1" />
                                        <input type="hidden" name="page" value="1" />
                          <input type="hidden" name="collection" value="main-collection" />
    </form>
               </div>

    </header>
		<nav id="mobile-sticky-header" class="color-base-plus-black-50 clearfix">
<div class="header-clear">
    <div id="sticky-logo"><a id="uni-home-link" href="//www.liverpool.ac.uk/" title="University of Liverpool Home">University of Liverpool</a></div>
    <div class="sticky-dept-link"><a href="//www.liverpool.ac.uk/strategy-2026">Strategy-2026</a></div>
    </div>
    

  </nav></div>

{{-- Pass the Book navigation --}}
<div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li ><a href="/" id="home">Home</a></li>
            <li><a href="/about" id="about">About</a></li>
            <li><a href="/contact" id="contact">Contact</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">

@include('partials.navbar-main')

          </ul>
 </div>
 {{-- end of Pass the Book navigation --}}

{{-- Pass the Book content --}}
  @if (Session::has('flash_notification.message'))
      <div class="alert alert-{{ Session::get('flash_notification.level') }}">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

          {!! Session::get('flash_notification.message') !!}
      </div>
  @endif

  
  @yield('content')

  @yield('errors')

{{-- end of Pass the Book content --}}

		<div class="footer">
		<div class="container">
    <div class="inner">
      <div id="address">
        <p class="vcard"> <span class="fn org">University of Liverpool</span><br>
          <span class="adr"> <span class="locality">Liverpool</span> <span class="postal-code">L69 3BX</span>, <span class="country-name">United Kingdom</span><br>
          </span> <span class="tel">+44 (0)151 794 2000</span> </p>
          
      </div>
    </div>
  </div>
  <div class="container ">
    <div class="inner copy-legal">
      <nav id="copyright">
        <ul>
          <li>© University of Liverpool - a member of <a href="https://www.russellgroup.ac.uk/" target="_blank">The Russell Group</a></li>
        </ul>
      </nav>
      <nav id="legal">
        <ul>
          <li><a href="https://www.liverpool.ac.uk/maps/">Map</a> </li>
          <li><a href="https://www.liverpool.ac.uk/legal/">Terms and Conditions</a></li>
          <li><a href="https://www.liverpool.ac.uk/accessibility/">Accessibility</a> </li>
        </ul>
      </nav>
      <div style="clear:both"></div>
    </div></div>
<!-- END ULIV HEADER -->
        </div>

	{{-- Pass the Book scripts --}}  
  <script>
    jQuery('#flash-overlay-modal').modal();
  </script>
  {{-- end of Pass the Book scripts --}}  
	
</body>

</html>
