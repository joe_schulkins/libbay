<nav id="mobile-sticky-header" class="color-base-plus-black-50 clearfix">
<div class="header-clear">
    <div id="sticky-logo"><a id="uni-home-link" href="//www.liverpool.ac.uk/" title="University of Liverpool Home">University of Liverpool</a></div>
    <div class="sticky-dept-link"><a href="//www.liverpool.ac.uk/strategy-2026">Strategy-2026</a></div>
    </div>
    

  </nav>