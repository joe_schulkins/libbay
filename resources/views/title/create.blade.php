@extends('app')

@section('content')
@if($title['isbns'] != false)

<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<h2>Add your book:</h2>
	</div>
</div>
<hr>
@include('title.find-detail')
@if($exists == false)
{!! Form::open(['action'=>'TitleDataController@addTitleItem']) !!}
@else
{!! Form::open(['action'=>'TitleDataController@addNewItem']) !!}
@endif			
@else
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<h2>Manually add your book:</h2>
	</div>
</div>
<hr>
{!! Form::open(['action'=>'TitleDataController@addTitleItem']) !!}
@include('title.form')
@endif
<div class="form-group price">
	<div class="row">
		<div class="col-xs-12 col-md-4">
			<div class="form-label">{!! Form::label('price','Price:') !!}</div>
		</div>
		<div class="col-xs-12 col-md-8">
			<div class="form-input">{!! Form::text('price',null, ['class'=> 'form-control','required' => 'required']) !!}</div>
		</div>
	</div>
</div>

<div class="form-group cond">
	<div class="row">
		<div class="col-xs-12 col-md-4">
			<div class="form-label">{!! Form::label('condition','Condition:') !!}</div>
		</div>
		<div class="col-xs-12 col-md-8">
			<div class="form-input">{!! Form::select('condition',['very_good'=>'Very Good','good'=>'Good','accpt'=>'Acceptable'], null, ['class'=> 'form-control','required' => 'required']) !!}</div>
		</div>
	</div>
</div>

<div class="form-group comments">
	<div class="row">
		<div class="col-xs-12 col-md-4">
			<div class="form-label">{!! Form::label('comments','Comments:') !!}</div>
		</div>
		<div class="col-xs-12 col-md-8">
			<div class="form-input">{!! Form::textarea('comments',null, ['class'=> 'form-control']) !!}</div>
		</div>
		
	</div>
</div>	
{!! Form::submit('Add Item', ['class'=> 'btn btn-primary form']) !!}
<a href="{{ url('/home') }}">Cancel</a>
{!! Form::close() !!}

</div>

@include('errors.list')

@stop

