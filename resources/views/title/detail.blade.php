<div class ="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="box" id="bib-imp-details">
			<div class="table">
				<table class="table">
					<tr>
						<td colspan="1">
							<i class="fa fa-4x fa-book"></i>
						</td>
						<td colspan="3">
							<div class="disp-main title">
								{{ ucwords($title->title) }}
							</div>
							@if($title->subtitle)
								<div class="disp-main subtitle">
									{{ $title->subtitle	 }}
								</div>
							@endif
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<table class="table table-striped">
								@if($title->authors)
									<tr>
										<div class="disp-title author">
											Author(s):
										</div>
									</tr>
									<tr>
										<div class="disp-body author">
											@foreach($title->authors as $value)
												{{ ucwords($value['lname']) }}, {{ ucwords($value['fname'])}}
											@endforeach
										</div>
									</tr>
								@endif

								@if($title->isbns)
									<tr>
										<div class="disp-title isbn">
											ISBN(s):
										</div>
									</tr>
									<tr>
										<div class="disp-body isbn">
											@foreach($title->isbns as $value)
												{{ $value['isbn'] }}
											@endforeach
										</div>
									</tr>
								@endif

								@if($title->pubdate)
									<tr>
										<div class="disp-title pubdate">
											Published Date:
										</div>
									</tr>
									<tr>
										<div class="disp-body pubdate">
			 								{{ $title->pubdate->year }}
										</div>
									</tr>
								@endif

								@if($title->edition)
									<tr>
										<div class="disp-title edition">
											Edition:
										</div>
									</tr>
									<tr>
										<div class="disp-body edition">
			 								{{ ucwords($title->edition) }}
			 							</div>
			 						</tr>
								@endif
							</table>
						</td>
						<td class="hidden-xs"><img src="http://www.syndetics.com/index.aspx?isbn={{ $title->isbns[0]['isbn'] }}/MC.GIF&amp;client=livea" alt=""></td>
						<td>
							<div class="btn-group center-block" role="group">
								@if($user != null)
								{!! Form::open(['action'=>'TitleDataController@findIsbn']) !!}
								{!! Form::hidden('isbn', $title->isbns[0]['isbn'] ) !!}
								{!! Form::submit('Sell your copy', ['class'=> 'btn btn-primary form center-block']) !!}
								{!! Form::close() !!}
								@else
								Please log in to sell your copy
								@endif
							</div>
							@if($admin == true)
							<a href="{{ route('titleEdit', $title->id) }}"><button type="button" class="btn btn-danger center-block">Edit this</button></a>
							@endif
						</td>
					</tr>
					<tr>
						<table class="table">
							@if($lists)
								<tr>
									<div class="disp-title reading">
										Reading lists:
									</div>
								</tr>
								@if(sizeof($lists) > 0)
									@foreach($lists as $value)
										<tr>
											<div class="rlist"><a href="{{ $value->rlist_url }}" >{{ $value->rlist_name }}</a></div>
										</tr>
									@endforeach
								@else
									<tr>
										<div class="no_rlist">There are no reading lists with this item.</div>
									</tr>
								@endif
							@endif
						</table>
					</tr>
					<tr>
						<table class="table table-striped">
							<caption>Items available:</caption>
							<thead>
								<tr>
									<th>Price</th>
									<th>Condition</th>
									<th>Comments</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								@foreach($title->items as $item)
								@if($item['attributes']['disp'] =='show')
								<tr>
									<td>&pound;{{ $item['attributes']['price'] }}</td>
									<td>{{ $item['attributes']['condition'] }}</td>
									<td>{{ $item['attributes']['comments'] }}</td>
									<td>
										<a href="/item/enquire/{{ $item['attributes']['id'] }}"><button type="button" class="btn btn-default btn-lg-text">Add to Basket <span  class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span></button></a>
									</td>
								</tr>
								@endif
								@endforeach
							</tbody>
						</table>
					</tr>
				</table>
				</div>
			</div>
		</div>
</div>