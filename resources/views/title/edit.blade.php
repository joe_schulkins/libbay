@extends('app')

@section('content')
	<h1>Edit: {{  $bib->title }}</h1>

	

	{!! Form::model($bib, ['method'=>'PATCH', 'action'=>['TitleDataController@update', $bib->id]]) !!}
		@include('title.form', ['submitButtonText' => 'Update Title'])
		<a href="{{ route('cancel') }}">Cancel</a>
	{!! Form::close() !!}




@stop