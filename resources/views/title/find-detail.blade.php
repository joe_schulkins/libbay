@if(gettype($title) == 'array')
	<div class ="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="box" id="bib-imp-details">
			<div class="table">
				<table class="table">
					<tr>
						<td colspan="1">
							<i class="fa fa-4x fa-book"></i>
						</td>
						<td colspan="3">
							<div class="disp-main title">
								{{ ucwords($title['title']) }}
							</div>
							@if($title['subtitle'])
								<div class="disp-main subtitle">
									{{ $title['subtitle']	 }}
								</div>
							@endif
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<table class="table table-striped">
								@if($title['authors'])
									<tr>
										<div class="disp-title author">
											Author(s):
										</div>
									</tr>
									<tr>
										<div class="disp-body author">
											@foreach($title['authors'] as $value)
												{{ ucwords($value['lname']) }}, {{ ucwords($value['fname'])}}
											@endforeach
										</div>
									</tr>
								@endif

								@if($title['isbns'])
									<tr>
										<div class="disp-title isbn">
											ISBN(s):
										</div>
									</tr>
									<tr>
										<div class="disp-body isbn">
											@foreach($title['isbns'] as $value)
												{{ $value }}
											@endforeach
										</div>
									</tr>
								@endif

								@if($title['pubdate'])
									<tr>
										<div class="disp-title pubdate">
											Published Date:
										</div>
									</tr>
									<tr>
										<div class="disp-body pubdate">
			 								{{ $title['pubdate'] }}
										</div>
									</tr>
								@endif

								@if($title['edition'] != '')
									<tr>
										<div class="disp-title edition">
											Edition:
										</div>
									</tr>
									<tr>
										<div class="disp-body edition">
			 								{{ ucwords($title['edition']) }}
			 							</div>
			 						</tr>
								@endif
							</table>
						</td>
						<td class="hidden-xs"><img src="http://www.syndetics.com/index.aspx?isbn={{ $title['isbns'][0] }}/MC.GIF&amp;client=livea" alt=""></td>
					</tr>
				</table>
				</div>
			</div>
		</div>
</div>
@else
<div class ="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="box" id="bib-imp-details">
			<div class="table">
				<table class="table">
					<tr>
						<td colspan="1">
							<i class="fa fa-4x fa-book"></i>
						</td>
						<td colspan="3">
							<div class="disp-main title">
								{{ ucwords($title->title) }}
							</div>
							@if($title->subtitle)
								<div class="disp-main subtitle">
									{{ $title->subtitle	 }}
								</div>
							@endif
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<table class="table table-striped">
								@if($title->authors)
									<tr>
										<div class="disp-title author">
											Author(s):
										</div>
									</tr>
									<tr>
										<div class="disp-body author">
											@foreach($title->authors as $value)
												{{ ucwords($value['lname']) }}, {{ ucwords($value['fname'])}}
											@endforeach
										</div>
									</tr>
								@endif

								@if($title->isbns)
									<tr>
										<div class="disp-title isbn">
											ISBN(s):
										</div>
									</tr>
									<tr>
										<div class="disp-body isbn">
											@foreach($title->isbns as $value)
												{{ $value['isbn'] }}
											@endforeach
										</div>
									</tr>
								@endif

								@if($title->pubdate)
									<tr>
										<div class="disp-title pubdate">
											Published Date:
										</div>
									</tr>
									<tr>
										<div class="disp-body pubdate">
			 								{{ $title->pubdate->year }}
										</div>
									</tr>
								@endif

								@if($title->edition)
									<tr>
										<div class="disp-title edition">
											Edition:
										</div>
									</tr>
									<tr>
										<div class="disp-body edition">
			 								{{ ucwords($title->edition) }}
			 							</div>
			 						</tr>
								@endif
							</table>
						</td>
						<td class="hidden-xs"><img src="http://www.syndetics.com/index.aspx?isbn={{ $title->isbns[0]['isbn'] }}/MC.GIF&amp;client=livea" alt=""></td>
					</tr>
				</table>
				</div>
			</div>
		</div>
</div>
@endif