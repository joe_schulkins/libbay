
	<div class="form-group title row">
		<div class="col-xs-12 col-md-4">
			{!! Form::label('title','Title:') !!}
		</div>
		<div class="col-xs-12 col-md-8">
			{!! Form::text('title',null, ['class'=> 'form-control']) !!}
		</div>
	</div>

	<div class="form-group subtitle row">
		<div class="col-xs-12 col-md-4">
			{!! Form::label('subtitle','Subtitle:') !!}
		</div>
		<div class="col-xs-12 col-md-8">
			{!! Form::text('subtitle',null, ['class'=> 'form-control']) !!}
		</div>
</div>

<div class='author-rows'>		
	@if($edit->authors)
	@foreach($edit->authors as $key => $value)
	<div class='form-group row'>
		<div class="col-xs-6 col-md-3">	
			{!! Form::label('lname','Last Name:', ['class'=>'col-sm-1 form-control-label']) !!}
		</div>
		<div class="col-xs-6 col-md-3">	
			{!! Form::text('author['.$key.'][lname]',$value->lname, ['class'=> 'form-control']) !!}
		</div>
		<div class="col-xs-6 col-md-3">	
			{!! Form::label('fname','First Name:', ['class'=>'col-sm-1 form-control-label']) !!}
		</div>
		<div class="col-xs-6 col-md-3">	
			{!! Form::text('author['.$key.'][fname]',$value->fname, ['class'=> 'form-control']) !!}
			{!! Form::hidden('author['.$key.'][id]',$value->id) !!}
		</div>
	</div>

	<div class="row">
		<div class="col-xs-6 col-md-4">
			<button type="button" class="btn btn-default btn-sm remove_author col-sm-1"><span class="glyphicon glyphicon-minus" aria-hidden="true" aria-label="remove row"></span></button>
		</div>
	</div>
	@endforeach
	@endif


</div>
	<div class="form-group row">
		<div class="col-xs-6 col-md-4">
			<button type="button" class="btn btn-default add_field_author">Add Authors <span class="glyphicon glyphicon-plus" aria-hidden="true" aria-label="add row"></span></button>	
		</div>
	</div> 


<div class='isbn-rows'>		
	@if($edit->isbns)
	@foreach($edit->isbns as $key => $value)
	<div class='form-group row'>
		<div class="col-xs-10">
			{!! Form::text('isbn['.$key.'][isbn]',$value->isbn, ['class'=> 'form-control']) !!}
		</div>
		{!! Form::hidden('isbn['.$key.'][id]',$value->id) !!}
		<div class="col-sm-2">
			<button type="button" class="btn btn-default btn-sm remove_isbn col-sm-1"><span class="glyphicon glyphicon-minus" aria-hidden="true" aria-label="remove row"></span></button>
		</div>
	</div>
	@endforeach
	@endif
</div>
<div class="form-group">
	<button type="button" class="btn btn-default add_field_isbn">Add ISBNs <span class="glyphicon glyphicon-plus" aria-hidden="true" aria-label="add row"></span></button>
</div>
<div class="form-group edition">
	<div class="row">
		<div class="col-xs-12 col-md-4">
			{!! Form::label('edition','Edition:') !!}
		</div>
		<div class="col-xs-12 col-md-8">
			{!! Form::text('edition',$edit->edition, ['class'=> 'form-control']) !!}
		</div>
	</div>
</div>
<div class="form-group volume">
	<div class="row">
		<div class="col-xs-12 col-md-4">
			{!! Form::label('volume','Volume:') !!}
		</div>
		<div class="col-xs-12 col-md-8">
			{!! Form::text('volume',$edit->volume, ['class'=> 'form-control']) !!}
		</div>
	</div>
</div>
<div class="form-group published">
	<div class="row">
		<div class="col-xs-12 col-md-4">
			{!! Form::label('pubdate','Date published:') !!}
		</div>
		<div class="col-xs-12 col-md-8">
			{!! Form::text('pubdate',$edit->pubdate->year, ['class'=> 'form-control pubdatepicker']) !!}
		</div>
	</div>
</div>