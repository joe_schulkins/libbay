@extends('app')

@section('content')
	<h1>Books Listed</h1>
	@if($search != null)
	{!! $titles->appends(['query' => $search])->render() !!}
	@else
	{!! $titles->render() !!}
	
	@endif
	<table class="table table-striped table-list">
	@foreach( $titles as $title)
		@if(is_array($title) == false )
			<?php $isbn = $title->isbns->first(); 
				if($isbn != null){
					$isbn->toArray();
				}
				else{
					$isbn = false;
				}
			?>

			@include('partials.index-obj')
		@else
			@foreach($title as $item)

				{{ dd($item[0]['attributes']) }}
			@endforeach
		@endif

	@endforeach
	</table>
	@if($search != null)
	{!! $titles->appends(['query' => $search])->render() !!}
	@else
	{!! $titles->render() !!}
	
	@endif
	
	


@stop

