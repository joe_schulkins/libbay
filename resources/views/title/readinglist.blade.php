@extends('app')

@section('content')
	
	<h1>Items on {{ $r->rlist_name }}</h1>
	{!! $col->render() !!}
	{{-- dd($col) --}}
	@foreach($col as $title)

		<div class="row">
			<div class="col-xs-12 col-md-10">
			@if($title['subtitle'])
				
					<div class="results-title">
						<a href="{{ url('/title', $title['id']) }}">{{ ucwords($title['title']) }}</a> :
					</div>
					<div class="results-subtitle">
						{{ $title['subtitle'] }}
					</div>
				
			@else
				
					<div class="results-title">
						<a href="{{ url('/title', $title['id']) }}">{{ ucwords($title['title']) }}</a>
					</div>
				
			@endif
				
					@if($title['edition'])
						{{ $title['edition'] }}
					@endif
				
					@if($title['volume'])
						{{ $title['volume'] }}
					@endif
				
					@if($title['pubdate'])
						{{ $title['pubdate']->year }}
					@endif
				</div>
				<div class="hidden-xs col-md-2">
					<img src="http://www.syndetics.com/index.aspx?isbn={{ $title['isbn'] }}/SC.GIF&amp;client=livea" alt="{{ $title['title'] }} book jacket" height="80px">
				</div>
			</div>
		@endforeach

@stop