@extends('app')

@section('content')
<h1>Add an item:</h1>
<div class="row">
	<div class="form col-sm-10">
		{!!  Form::open(['action'=>'TitleDataController@findIsbn']) !!}
		<div class="form-group">
			<div class="isbn_fields_wrap">
				{!! Form::label('isbn','ISBN:', ['class'=> 'col-sm-1 form-control-label isbn'] ) !!}
				<div class="col-sm-8">
					{!! Form::text('isbn',null, ['class'=> 'form-control isbn']) !!}
				</div>
			</div>
		</div>
		<div class="form-group submit">
			{!! Form::submit('Add an item', ['class'=> 'btn btn-primary form']) !!}
			<a href="{{ action('TitleDataController@createManualTitle') }}"><button type="button" class="btn btn-success">No ISBN</button></a>
			<a href="{{ route('cancel') }}">Cancel</a>
		</div>
		
		{!! Form::close() !!}
		
	</div>
</div>



@include('errors.list')

@stop