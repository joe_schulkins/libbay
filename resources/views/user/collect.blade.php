@extends('app')

@section('content')	

@include('title.find-detail')

	<table class="table table-condensed borderless">
		{!! Form::open(['action'=>['ItemDataController@setCollectAttr', $item->id]]) !!}
		<tr>
			<td colspan="1">{!! Form::label('date','Set pick up date:') !!}</td>
			<td colspan="5">{!! Form::text('date', '', array('class' => 'datepicker form-control')) !!}</td>
		</tr>
		<tr>
			<td colspan="2"></td>
			<td colspan="2"></td>
			<td align="right">{!! Form::submit('Submit', ['class'=> 'btn btn-primary form']) !!}</td>
			<td align="left"><button class="btn btn-default"><a href="{{ route('cancel') }}">Cancel</a></button></td>
			{!! Form::close() !!}
		</tr>
		<tr>
			@include('partials.text-collect')
		</tr>
	</table>

@stop