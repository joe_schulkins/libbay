@extends('app')

@section('content')	

<h1>Users enquiring about '{{ ucwords($title[0]->title) }}'</h1>

<table class="table table-striped">
<?php $i = 1; ?>
@foreach($enq_a as $user)

	<tr>
		<td>{{ $i }}</td>
		<td> Item number: {{  $user['eid'] }}</td>
		<td>Date and time of enquiry {{ $user['time']}}</td>
		<td><a href="{{ url('/user/enq-approve', [$user['user'], $user['eid'], ucwords($title[0]->title)]) }}"><button type="submit" class="btn btn-success">Accept</button></a><a href="{{ url('/user/enq-reject', [$user['user'], $user['eid'], ucwords($title[0]->title)]) }}"><button type="submit" class="btn btn-danger">Reject</button></a></td>
	</tr>
<?php $i++; ?>
@endforeach
</table>
<a href="{{ url('/user/enq-reject', $user['user']) }}"><button type="submit" class="btn btn-danger">Reject All</button></a>
@stop