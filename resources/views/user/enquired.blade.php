@extends('app')

@section('content')	



<div class="panel panel-default">

	<div class="panel-heading">Books you are buying:</div>

	<div class="panel-body">

		<table class="table table-ellip table-striped ">
			<thead>
				<tr>
					<th>Title</th>
					<th>Price</th>
					<th>Condition</th>
					<th>Comments</th>
					<th></th>
					<th>Date Last Change</th>
					<th></th>
					<th colspan="4">Action</th>
				</tr>
			</thead>
			<tbody>
				
					@foreach($enquired as $titles)
					
						<tr>
							<td><a href="{{ url('/title', $titles->title[0]->id) }}">{{ ucwords($titles->title[0]->title) }}</a></td>
							<td>&pound;{{ $titles->price }}</td>
							<td>{{ ucwords($titles->condition) }}</td>
							<td>{{ ucwords($titles->comments) }}</td>
							<td></td>
							<td>{{ $titles->updated_at->format('d/m/Y') }}</td>
							<td></td>
							<td colspan="4">
							@if($titles->status == 'purchased')
							<a href="{{ route('cancelItem', [$titles['attributes']['id'], $user->id]) }}"><button type="submit" class="btn btn-info">Cancel</button></a>
							<a href="{{ url('/item/collect', $titles['attributes']['id']) }}"><button type="submit" class="btn">Collect</button></a>
							@elseif($titles->status == 'collect')
							<button type="submit" class="btn btn-info disabled">Cancel</button>
							<a href="{{ route('printSlip', $titles['attributes']['id']) }}"><button type="submit" class="btn btn-info">Collection Receipt</button></a>
							@endif
							{{-- <a href="{{ url('/item/message', $titles['attributes']['id']) }}"><button type="submit" class="btn btn-success">Message</button></a> --}}
							</td>
						</tr>
						
					@endforeach

			</tbody>
		</table>
	</div>
</div>

@stop