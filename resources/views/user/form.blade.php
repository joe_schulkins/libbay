<div class='name-rows'>
<div class='form-group row'>	
	{!! Form::label('fname','First Name:', ['class'=>'col-sm-1 form-control-label']) !!}
	<div class="col-sm-4">
		{!! Form::text('fname',null, ['class'=> 'form-control']) !!}
	</div>
	{!! Form::label('lname','Last Name:', ['class'=>'col-sm-1 form-control-label']) !!}
	<div class="col-sm-4">
		{!! Form::text('lname',null, ['class'=> 'form-control']) !!}
	</div>
</div>
<div class='form-group row'>	
	{!! Form::label('email','Email:', ['class'=>'col-sm-1 form-control-label']) !!}
	<div class="col-sm-4">
		{!! Form::text('email',null, ['class'=> 'form-control']) !!}
	</div>
</div>
<div class='form-group row'>	
	{!! Form::label('pay','Payment details:', ['class'=>'col-sm-1 form-control-label']) !!}
	<div class="col-sm-4">
		{!! Form::text('pay',null, ['class'=> 'form-control']) !!}
	</div>
</div>