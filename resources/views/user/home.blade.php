@extends('app')

@section('content')	

<h2>User dashboard</h2>

<div class="row">
	{{--<div class="col-xs-2 col-md-4">
	<div class="panel panel-success">
		<div class="panel-heading"><a href="{{ route('allNotifications', $user->id) }}" id="notifications">New Notifications</a></div>
		<div class="panel-body">
			@if($user->countNotificationsNotRead() > 0)
			<a href="{{ route('allNotifications', $user->id) }}" id="notifications">{{$user->countNotificationsNotRead()}}</a>
			@else
			No new notifications.
			@endif
		</div>
	</div>
</div>	--}}
<div class="col-xs-2 col-md-4">
	<div class="panel panel-info">
		<div class="panel-heading"><a href="{{ route('listed', $user->id) }}">Books you are selling</a></div>
		<div class="panel-body">
			@if(count($user->listed) > 0)
			<a href="{{ route('listed', $user->id) }}">{{ count($user->listed)  }}</a>
			@else
			{{ count($user->listed)  }}					
			@endif
		</div>
	</div>
</div>
<div class="col-xs-2 col-md-4">
	<div class="panel panel-info">
		<div class="panel-heading"><a href="{{ route('enquired',  $user->id) }}">Books you are buying</a></div>
		<div class="panel-body">
			@if(count($user->enquired) > 0)
			<a href="{{ route('enquired',  $user->id) }}">{{ count($user->enquired)  }}</a>
			@else
			{{ count($user->enquired)  }}
			@endif
		</div>
	</div>
</div>
</div>

<div class="well">
	<div class="btn-group" role="group">
		<a href="/title/create"><button type="button" class="btn btn-primary">Add Item</button></a>
	</div>
</div>


@stop