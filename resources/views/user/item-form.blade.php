{{-- dd($edit) --}}
<div class="form-group ref">
	<div class="row">
	<div class="col-xs-6">{!! Form::label('ref','Reference Number:') !!}</div>
	<div class="col-xs-6">{!! Form::text('ref',null, ['class'=> 'form-control', 'readonly']) !!}</div>
	</div>
</div>
<div class="form-group condition">
<div class="row">
	<div class="col-xs-6">{!! Form::label('condition','Condition:') !!}</div>
	<div class="col-xs-6">{!! Form::select('condition',['very_good'=>'Very Good','good'=>'Good','accpt'=>'Acceptable'], $edit->condition, ['class'=> 'form-control']) !!}</div>
	</div>
</div>
<div class="form-group comments">
	<div class="row">
	<div class="col-xs-6">{!! Form::label('comments','Comments:') !!}</div>
	<div class="col-xs-6">{!! Form::textarea('comments',null, ['class'=> 'form-control']) !!}</div>
	</div>
</div>
<div class="form-group price">
	<div class="row">
	<div class="col-xs-6">{!! Form::label('price','Price:') !!}</div>
	<div class="col-xs-6">{!! Form::text('price',null, ['class'=> 'form-control']) !!}</div>
	</div>
</div>
<div class="form-group status">
	<div class="row">
	<div class="col-xs-6">{!! Form::label('status','Status:') !!}</div>
	@if($edit->status == 'collect')
	<div class="col-xs-6">{!! Form::text('status',null, ['class'=> 'form-control', 'readonly']) !!}</div>
	@else
	<div class="col-xs-6">{!! Form::select('status',['listed'=>'Listed','purchased'=>'Purchased','collect'=>'Collect', 'review' => 'Review', 'finished' => 'Finished', 'cleared' => 'Deleted'], $edit->status, ['class'=> 'form-control']) !!}</div>
	@endif
	</div>
</div> 
<div class="form-group Collect">
	<div class="row">
	<div class="col-xs-6">{!! Form::label('collect','Collect:') !!}</div>
	@if($edit->status == 'collect')
	<div class="col-xs-6">{!! Form::text('collect',$edit->collect->format('d-m-Y'), ['class'=> 'form-control datepicker']) !!}</div>
	@else
	<div class="col-xs-6">{!! Form::text('collect',null, ['class'=> 'form-control datepicker']) !!}</div>
	@endif
	</div>
</div>
<div class="form-group delete">
	<div class="row">
	<div class="col-xs-6">{!! Form::label('deleted','Deleted on:') !!}</div>
	@if($edit->deleted_at != null)
	<div class="col-xs-6">{!! Form::text('delete',$edit->deleted_at->format('d-m-Y'), ['class'=> 'form-control datepicker']) !!}</div>
	@else
	<div class="col-xs-6">{!! Form::text('delete',null, ['class'=> 'form-control datepicker']) !!}</div>
	@endif
	</div>
</div>
