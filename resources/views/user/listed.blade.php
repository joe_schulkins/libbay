@extends('app')

@section('content')	


<div class="panel panel-default">

	<div class="panel-heading">Books you are selling:</div>

	<div class="panel-body">

		<table class="table table-ellip table-striped ">
			<thead>
				<tr>
					<th>Title</th>
					<th>Price</th>
					<th>Condition</th>
					<th>Comments</th>
					<th>Status</th>
					<th>Date Last Change</th>
					{{-- <th>Enquiries</th> --}}
					<th colspan="4">Action</th>
				</tr>
			</thead>
			<tbody>
				
				@foreach($listed as $titles)
				<tr>
					<td><a href="{{ url('/title', $titles->title[0]->id) }}">{{ ucwords($titles->title[0]->title) }}</a></td>
					<td>&pound;{{ $titles->price }}</td>
					<td>{{ ucwords($titles->condition) }}</td>
					<td>{{ ucwords($titles->comments) }}</td>
					<td>{{ ucwords($titles->status) }}</td>
					<td>{{ $titles->updated_at->format('d/m/Y') }}</td>
					{{-- <td>{{ $titles['relations']['enquired']->count()}}</td> --}}
					<td colspan="4">
						@if($titles->disp != 'hide')
						<a href="{{ url('/item/hide', $titles['attributes']['id']) }}"><button type="submit" class="btn btn-warning">Hide</button></a>
						@elseif($titles->status == 'collect' || $titles->status == 'purchased')
						<a href="{{ url('/item/hide', $titles['attributes']['id']) }}"><button type="submit" class="btn btn-warning disabled">Show</button></a>
						@else
						<a href="{{ url('/item/hide', $titles['attributes']['id']) }}"><button type="submit" class="btn btn-warning">Show</button></a>
						@endif							
						<a href="{{ route('printSlip', $titles['attributes']['id']) }}"><button type="submit" class="btn btn-info">Cover Sheet</button></a>						
						@if($titles['attributes']['status'] == 'listed' ||$titles['attributes']['status'] == 'review')
						<a href="{{ url('/item/remove', $titles['attributes']['id']) }}"><button type="submit" class="btn btn-danger">Remove</button></a>
						@endif
						{{-- <a href="{{ url('/item/message', $titles['attributes']['id']) }}"><button type="submit" class="btn btn-success">Message</button></a> --}}
					</td>
				</tr>

				@endforeach
				
			</tbody>
		</table>
	</div>
</div>
@stop