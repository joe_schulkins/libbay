<!-- this is a master layout file for our blade template and all the content served up by our views will be placed at 'content' -->

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>{{ Config::get('bosbooks.name') }}</title>

	@include('templates.livuni.header')

	<link rel="stylesheet" href="{{ elixir('css/app.css') }}">
	<link rel="stylesheet" href="/css/jquery-ui/themes/smoothness/jquery-ui.min.css">
	<script type="text/javascript" 	src="{{  elixir('js/vendor.js') }}"></script>
</head>
<body>

<div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li ><a href="/" id="home">Home</a></li>
            <li><a href="/about" id="about">About</a></li>
            <li><a href="/contact" id="contact">Contact</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">

@include('partials.navbar-main')

          </ul>
 </div>



	<div class="container">

	<div class="panel panel-danger">
      <div class="panel-heading">Please Note</div>
      <div class="panel-body">This is a work in progress. This site is liable to changes and erasures of data.</div>
    </div>

	@if (Session::has('flash_notification.message'))
    	<div class="alert alert-{{ Session::get('flash_notification.level') }}">
        	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

        	{!! Session::get('flash_notification.message') !!}
    	</div>
	@endif

	
	@yield('content')

	@yield('errors')
	
	@include('templates.livuni.footer')
	
	<script>
		jQuery('#flash-overlay-modal').modal();
	</script>
	</div>
</body>
</html>