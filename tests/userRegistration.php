<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class userRegistration extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {

    	Honeypot::disable();

    	$this->visit('/auth/register')
         ->type('Charlie', 'fname')
         ->type('Brown', 'lname')
         ->type('1111111', 'uni_id')
         ->type('snoopy@woodstock.com', 'email')
         ->type('password', 'password')
         ->type('password', 'password_confirmation')
         ->check('terms_conditions')
         ->press('Register')
         ->seePageIs('/home');
    }
}
